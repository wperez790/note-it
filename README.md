# Note It
---

Repositorio con los distintos módulos de la aplicación `Note It`, 
perteneciente al Trabajo Integrador de la Materia **Programación Funcional y Scripting** 2020

### Profesor: Ing. Mariano Garcia Mattio

## Modulo de Backend
---

Directorio `back/`

Desarrollado por **Nicolás Gomez**

## Modulo de Frontend
---

Directorio `front/`

Desarrollado por **Walter Perez Sardi**

## Modulo de Node-RED
---

Directorio `node-red/`

Desarrollado por **Iván Guerra**