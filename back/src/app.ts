// lib/app.ts
import express = require('express');
import fileUpload = require('express-fileupload');
import { StatusCodes } from 'http-status-codes';
import 'reflect-metadata';
import { BusinessException } from './Exceptions/BusinessException';
import { AuthorizationFilter } from './filter/AuthorizationFilter';
import { ApiRoute } from './routes/ApiRouter';

// Create a new express application instance
const app: express.Application = express();

async function init() {
  // File upload
  app.use(
    fileUpload({
      createParentPath: true
    })
  );

  // CORS
  app.use((_req, res, next) => {
    // authorized headers for preflight requests
    // https://developer.mozilla.org/en-US/docs/Glossary/preflight_request
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
    next();

    app.options('*', () => {
      // allowed XHR methods
      res.header('Access-Control-Allow-Methods', 'GET, PATCH, PUT, POST, DELETE, OPTIONS');
      res.send();
    });
  });

  // Parseo de body
  app.use(express.json());

  // Establenciendo el filtro por token
  app.use(AuthorizationFilter);

  const api = await ApiRoute.getInstance();
  app.use('/api', api.getRouter());

  // Manejando errores
  app.use((err: Error, _req: express.Request, res: express.Response, _next) => {
    if (err instanceof BusinessException) {
      res.status(err.resCode).send(err.msg);
      return;
    }

    console.error(err.message, '\n' + err.stack);

    res.status(StatusCodes.INTERNAL_SERVER_ERROR).send();
  });

  // Iniciando el server
  app.listen(3000, () => {
    console.clear();
    console.log('Example app listening on port 3000!');
  });
}

init();
