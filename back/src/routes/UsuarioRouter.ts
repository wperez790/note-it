import { Request, Response } from 'express';
import { StatusCodes } from 'http-status-codes';
import { UserBusiness } from '../business/UserBusiness';
import { ApiRouter } from '../model/ApiRouter';

export class UsuarioRoute extends ApiRouter {
  private userBusiness: UserBusiness;

  // ------------------- REST --------------------
  private async createUser(req: Request, res: Response) {
    if (req.body == null) {
      res.status(StatusCodes.BAD_REQUEST).send('Body missing!');
      return;
    }

    await this.userBusiness.createUser(req.body);
    res.status(StatusCodes.CREATED).send();
  }

  private async updateUser(req: Request, res: Response) {
    if (req.body == null) {
      res.status(StatusCodes.BAD_REQUEST).send('Body missing!');
      return;
    }

    let token = req.header('Authorization')?.replace(/Bearer ?/g, '');
    if (token == null) {
      res.status(StatusCodes.UNAUTHORIZED).send();
      return;
    }

    token = await this.userBusiness.updateUser(req.body, token);

    res.status(StatusCodes.OK).send({ token: token });
  }

  private async findUser(req: Request, res: Response) {
    if (req.body == null) {
      res.status(StatusCodes.BAD_REQUEST).send('Body missing!');
      return;
    }

    const searchParam = decodeURI(req.params.value);
    if (searchParam == null || searchParam === '') {
      res.status(StatusCodes.BAD_REQUEST).send('Invalid search');
    }

    const token = req.header('Authorization')?.replace(/Bearer ?/g, '');
    if (token == null) {
      res.status(StatusCodes.UNAUTHORIZED).send();
      return;
    }
    const users = await this.userBusiness.findUser(searchParam, token);

    res.status(StatusCodes.OK).send(users);
  }

  private async getUserInformation(req: Request, res: Response) {
    const token = req.header('Authorization')?.replace(/Bearer ?/g, '');
    if (token == null) {
      res.status(StatusCodes.UNAUTHORIZED).send();
      return;
    }

    const user = await this.userBusiness.getUserInformationByToken(token);
    delete user.emailCheck;

    res.status(StatusCodes.OK).send(user);
  }

  private async getUserById(req: Request, res: Response) {
    const token = req.header('Authorization')?.replace(/Bearer ?/g, '');
    if (token == null) {
      res.status(StatusCodes.UNAUTHORIZED).send();
      return;
    }

    const userId = decodeURI(req.params.userId);
    if (userId == null || userId === '') {
      res.status(StatusCodes.BAD_REQUEST).send('UserIdRequired');
    }

    const user = await this.userBusiness.getUserById(userId, token);
    delete user.emailCheck;

    res.status(StatusCodes.OK).send(user);
  }

  // ------------------- SINGLETON --------------------
  private static instance: UsuarioRoute;
  private constructor() {
    super();
  }

  private async init() {
    this.userBusiness = await UserBusiness.getInstance();

    // Manejo de operaciones /api/usuario

    this.router.get('/:userId', (req, res, next) => {
      this.getUserById(req, res).catch(next);
    });

    this.router.get('/', (req, res, next) => {
      this.getUserInformation(req, res).catch(next);
    });

    this.router.post('/', (req, res, next) => {
      this.createUser(req, res).catch(next);
    });

    this.router.put('/', (req, res, next) => {
      this.updateUser(req, res).catch(next);
    });

    this.router.get('/like/:value', (req, res, next) => {
      this.findUser(req, res).catch(next);
    });
  }

  static async getInstance() {
    if (UsuarioRoute.instance == null) {
      UsuarioRoute.instance = new UsuarioRoute();
      await UsuarioRoute.instance.init();
    }

    return UsuarioRoute.instance;
  }
}
