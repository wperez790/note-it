import { Request, Response } from 'express';
import { StatusCodes } from 'http-status-codes';
import { FileRouter } from './FileRouter';
import { NotebookRoute } from './NotebookRouter';
import { NoteRoute } from './NoteRouter';
import { ApiRouter } from '../model/ApiRouter';
import { UserBusiness } from '../business/UserBusiness';
import { User } from '../model/User';
import { UsuarioRoute } from './UsuarioRouter';

export class ApiRoute extends ApiRouter {
  private userBusiness: UserBusiness;

  // ------------------- REST --------------------
  async loginRest(req: Request, res: Response) {
    if (req.body == null) {
      res.status(StatusCodes.BAD_REQUEST).send('Body missing!');
      return;
    }

    const user = new User().copy(req.body);

    const token = await this.userBusiness.login(user);

    if (token == null) {
      res.status(StatusCodes.UNAUTHORIZED).send();
    } else {
      res.status(StatusCodes.OK).send({ token: token });
    }
  }

  async requestRestorePassword(req: Request, res: Response) {
    if (req.body == null) {
      res.status(StatusCodes.BAD_REQUEST).send('Body missing!');
      return;
    }

    const user = new User().copy(req.body);

    await this.userBusiness.requestRestorePassword(user);

    res.status(StatusCodes.OK).send();
  }

  async restorePassword(req: Request, res: Response) {
    if (req.body == null) {
      res.status(StatusCodes.BAD_REQUEST).send('Body missing!');
      return;
    }

    const user = new User().copy(req.body);

    await this.userBusiness.restorePassword(user);

    res.status(StatusCodes.OK).send();
  }

  // ------------------- SINGLETON --------------------
  private static instance: ApiRoute;
  private constructor() {
    super();
  }

  private async init() {
    this.userBusiness = await UserBusiness.getInstance();

    this.router.use('/libreta', (await NotebookRoute.getInstance()).getRouter());
    this.router.use('/nota', (await NoteRoute.getInstance()).getRouter());
    this.router.use('/usuario', (await UsuarioRoute.getInstance()).getRouter());
    this.router.use('/archivo', (await FileRouter.getInstance()).getRouter());

    // Manejo de operaciones /api/
    this.router.post('/login', (req, res, next) => {
      this.loginRest(req, res).catch(next);
    });

    this.router.post('/restore-password', (req, res, next) => {
      this.requestRestorePassword(req, res).catch(next);
    });

    this.router.post('/change-password', (req, res, next) => {
      this.restorePassword(req, res).catch(next);
    });
  }

  static async getInstance() {
    if (ApiRoute.instance == null) {
      ApiRoute.instance = new ApiRoute();
      await ApiRoute.instance.init();
    }

    return ApiRoute.instance;
  }
}
