import { Request, Response } from 'express';
import { StatusCodes } from 'http-status-codes';
import { FileBusiness } from '../business/FileBusiness';
import { NoteBusiness } from '../business/NoteBusiness';
import { ApiRouter } from '../model/ApiRouter';

export class NoteRoute extends ApiRouter {
  public noteBusiness: NoteBusiness;
  public fileBusiness: FileBusiness;

  private async createNote(req: Request, res: Response) {
    if (req.body == null) {
      res.status(StatusCodes.BAD_REQUEST).send('Body missing!');
      return;
    }

    const token = req.header('Authorization')?.replace(/Bearer ?/g, '');
    if (token == null) {
      res.status(StatusCodes.UNAUTHORIZED).send();
      return;
    }

    const id = await this.noteBusiness.createNote(req.body, token);
    res.status(StatusCodes.CREATED).send({ id: id });
  }

  private async updateNote(req: Request, res: Response) {
    if (req.body == null) {
      res.status(StatusCodes.BAD_REQUEST).send('Body missing!');
      return;
    }

    const token = req.header('Authorization')?.replace(/Bearer ?/g, '');
    if (token == null) {
      res.status(StatusCodes.UNAUTHORIZED).send();
      return;
    }

    if (await this.noteBusiness.updateNote(req.body, token)) {
      res.status(StatusCodes.OK).send();
    } else {
      res.status(StatusCodes.INTERNAL_SERVER_ERROR);
    }
  }

  private async getNotes(req: Request, res: Response) {
    if (req.body == null) {
      res.status(StatusCodes.BAD_REQUEST).send('Body missing!');
      return;
    }

    const token = req.header('Authorization')?.replace(/Bearer ?/g, '');
    if (token == null) {
      res.status(StatusCodes.UNAUTHORIZED).send();
      return;
    }

    const notebookId = req.query.id_libreta as string;
    if (notebookId != null && notebookId === '') {
      res.status(StatusCodes.BAD_REQUEST).send('Invalid Notebook Id');
      return;
    }

    let notes;
    if (notebookId == null) {
      notes = await this.noteBusiness.getOwnNotes(token);
    } else {
      notes = await this.noteBusiness.getNotebookNotes(notebookId, token);
    }
    res.status(StatusCodes.OK).send(notes);
  }

  private async getSharedNotes(req: Request, res: Response) {
    if (req.body == null) {
      res.status(StatusCodes.BAD_REQUEST).send('Body missing!');
      return;
    }

    const token = req.header('Authorization')?.replace(/Bearer ?/g, '');
    if (token == null) {
      res.status(StatusCodes.UNAUTHORIZED).send();
      return;
    }

    const notes = await this.noteBusiness.getSharedNotes(token);
    res.status(StatusCodes.OK).send(notes);
  }

  private async uploadFile(req: Request, res: Response) {
    const reqAny: any = req;
    if (reqAny.files.file == null) {
      res.status(StatusCodes.BAD_REQUEST).send('Form Data field "File" required');
    }

    const noteId = decodeURI(req.params.noteId);
    if (noteId == null || noteId === '') {
      res.status(StatusCodes.BAD_REQUEST).send('Invalid noteId');
    }

    const token = req.header('Authorization')?.replace(/Bearer ?/g, '');
    if (token == null) {
      res.status(StatusCodes.UNAUTHORIZED).send();
      return;
    }

    const id = await this.fileBusiness.uploadFileToNote(reqAny.files.file, noteId, token);
    res.status(StatusCodes.CREATED).send({ id: id });
  }

  private async getNoteParticipants(req: Request, res: Response) {
    const token = req.header('Authorization')?.replace(/Bearer ?/g, '');
    if (token == null) {
      res.status(StatusCodes.UNAUTHORIZED).send();
      return;
    }

    const noteId = decodeURI(req.params.noteId);
    if (noteId == null || noteId === '') {
      res.status(StatusCodes.BAD_REQUEST).send('Invalid noteId');
    }

    const users = await this.noteBusiness.getNoteParticipants(noteId, token);
    res.status(StatusCodes.OK).send(users);
  }

  private async getFiles(req: Request, res: Response) {
    const noteId = decodeURI(req.params.noteId);
    if (noteId == null || noteId === '') {
      res.status(StatusCodes.BAD_REQUEST).send('Invalid noteId');
    }

    const token = req.header('Authorization')?.replace(/Bearer ?/g, '');
    if (token == null) {
      res.status(StatusCodes.UNAUTHORIZED).send();
      return;
    }

    const files = await this.fileBusiness.getFilesOfNote(noteId, token);
    res.status(StatusCodes.OK).send(files);
  }

  private async getNoteById(req: Request, res: Response) {
    const token = req.header('Authorization')?.replace(/Bearer ?/g, '');
    if (token == null) {
      res.status(StatusCodes.UNAUTHORIZED).send();
      return;
    }

    const noteId = decodeURI(req.params.noteId);
    if (noteId == null || noteId === '') {
      res.status(StatusCodes.BAD_REQUEST).send('Invalid noteId');
    }

    const note = await this.noteBusiness.getNoteById(noteId, token);
    res.status(StatusCodes.OK).send(note);
  }

  private async deleteNote(req: Request, res: Response) {
    const token = req.header('Authorization')?.replace(/Bearer ?/g, '');
    if (token == null) {
      res.status(StatusCodes.UNAUTHORIZED).send();
      return;
    }

    const noteId = decodeURI(req.params.noteId);
    if (noteId == null || noteId === '') {
      res.status(StatusCodes.BAD_REQUEST).send('Invalid noteId');
    }
    await this.noteBusiness.deleteNoteById(noteId, token);
    res.status(StatusCodes.OK).send();
  }

  // ------------------- SINGLETON --------------------
  private static instance: NoteRoute;
  private constructor() {
    super();
  }

  private async init() {
    this.noteBusiness = await NoteBusiness.getInstance();
    this.fileBusiness = await FileBusiness.getInstance();

    // Manejo de operaciones /api/nota

    // Obtener los participantes de una nota
    this.router.get('/:noteId/participantes', (req, res, next) => {
      this.getNoteParticipants(req, res).catch(next);
    });

    // Obtener archivos de una nota
    this.router.get('/:noteId/archivo', (req, res, next) => {
      this.getFiles(req, res).catch(next);
    });

    // Obtener las notas compartidas sin agrupar
    this.router.get('/shared', (req, res, next) => {
      this.getSharedNotes(req, res).catch(next);
    });

    // Obtener las notas compartidas sin agrupar
    this.router.get('/:noteId', (req, res, next) => {
      this.getNoteById(req, res).catch(next);
    });

    // Obtener las notas del usuario sin agrupar
    this.router.get('/', (req, res, next) => {
      this.getNotes(req, res).catch(next);
    });

    // Creación de la nota
    this.router.post('/', (req, res, next) => {
      this.createNote(req, res).catch(next);
    });

    // Endpoint carga de archivo
    this.router.post('/:noteId/archivo', (req, res, next) => {
      this.uploadFile(req, res).catch(next);
    });

    // Actualizar Nota
    this.router.put('/', (req, res, next) => {
      this.updateNote(req, res).catch(next);
    });

    // Eliminar Nota
    this.router.delete('/:noteId', (req, res, next) => {
      this.deleteNote(req, res).catch(next);
    });
  }

  static async getInstance() {
    if (NoteRoute.instance == null) {
      NoteRoute.instance = new NoteRoute();
      await NoteRoute.instance.init();
    }

    return NoteRoute.instance;
  }
}
