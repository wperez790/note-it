import { Request, Response } from 'express';
import { StatusCodes } from 'http-status-codes';
import { NotebookBusiness } from '../business/NotebookBusiness';
import { ApiRouter } from '../model/ApiRouter';

export class NotebookRoute extends ApiRouter {
  public libretaBusiness: NotebookBusiness;

  private async createNotebook(req: Request, res: Response) {
    if (req.body == null) {
      res.status(StatusCodes.BAD_REQUEST).send('Body missing!');
      return;
    }

    const token = req.header('Authorization')?.replace(/Bearer ?/g, '');
    if (token == null) {
      res.status(StatusCodes.UNAUTHORIZED).send();
      return;
    }

    const id = await this.libretaBusiness.createNotebook(req.body, token);
    res.status(StatusCodes.CREATED).send({ id: id });
  }

  private async updateNotebook(req: Request, res: Response) {
    if (req.body == null) {
      res.status(StatusCodes.BAD_REQUEST).send('Body missing!');
      return;
    }

    const token = req.header('Authorization')?.replace(/Bearer ?/g, '');
    if (token == null) {
      res.status(StatusCodes.UNAUTHORIZED).send();
      return;
    }

    await this.libretaBusiness.updateNotebook(req.body, token);
    res.status(StatusCodes.OK).send();
  }

  private async getNotebooks(req: Request, res: Response) {
    const token = req.header('Authorization')?.replace(/Bearer ?/g, '');
    if (token == null) {
      res.status(StatusCodes.UNAUTHORIZED).send();
      return;
    }

    const libretas = await this.libretaBusiness.getNotebook(token);
    res.status(StatusCodes.OK).send(libretas);
  }

  private async getSharedNotebooks(req: Request, res: Response) {
    const token = req.header('Authorization')?.replace(/Bearer ?/g, '');
    if (token == null) {
      res.status(StatusCodes.UNAUTHORIZED).send();
      return;
    }

    const libretas = await this.libretaBusiness.getSharedNotebook(token);
    res.status(StatusCodes.OK).send(libretas);
  }

  private async deleteNotebook(req: Request, res: Response) {
    const token = req.header('Authorization')?.replace(/Bearer ?/g, '');
    if (token == null) {
      res.status(StatusCodes.UNAUTHORIZED).send();
      return;
    }

    const notebookId = decodeURI(req.params.notebookId);
    if (notebookId == null || notebookId === '') {
      res.status(StatusCodes.BAD_REQUEST).send('Invalid noteId');
    }
    await this.libretaBusiness.deleteNotebookById(notebookId, token);
    res.status(StatusCodes.OK).send();
  }

  // ------------------- SINGLETON --------------------
  private static instance: NotebookRoute;
  private constructor() {
    super();
  }

  private async init() {
    this.libretaBusiness = await NotebookBusiness.getInstance();

    // Manejo de operaciones /api/libreta
    this.router.post('/', (req, res, next) => {
      this.createNotebook(req, res).catch(next);
    });

    this.router.put('/', (req, res, next) => {
      this.updateNotebook(req, res).catch(next);
    });

    this.router.get('/', (req, res, next) => {
      this.getNotebooks(req, res).catch(next);
    });

    this.router.get('/shared', (req, res, next) => {
      this.getSharedNotebooks(req, res).catch(next);
    });

    this.router.delete('/:notebookId', (req, res, next) => {
      this.deleteNotebook(req, res).catch(next);
    });
  }

  static async getInstance() {
    if (NotebookRoute.instance == null) {
      NotebookRoute.instance = new NotebookRoute();
      await NotebookRoute.instance.init();
    }

    return NotebookRoute.instance;
  }
}
