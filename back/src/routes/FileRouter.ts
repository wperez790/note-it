import { Request, Response } from 'express';
import { StatusCodes } from 'http-status-codes';
import { FileBusiness } from '../business/FileBusiness';
import { ApiRouter } from '../model/ApiRouter';

export class FileRouter extends ApiRouter {
  private userBusiness: FileBusiness;

  // ------------------- REST --------------------
  private async dowloadFile(req: Request, res: Response) {
    const token = req.header('Authorization')?.replace(/Bearer ?/g, '');
    if (token == null) {
      res.status(StatusCodes.UNAUTHORIZED).send();
      return;
    }
    const fileId = decodeURI(req.params.fileId);
    if (fileId == null || fileId === '') {
      res.status(StatusCodes.BAD_REQUEST).send('Invalid search');
    }

    const file = await this.userBusiness.getFile(fileId, token);

    // console.log(file);
    res.attachment(file.nombre);
    res.contentType(file.mimeType);
    res.setHeader('Content-Length', file.tamaño);
    res.download(file.path, file.nombre, (err) => {
      if (err) {
        res.status(StatusCodes.INTERNAL_SERVER_ERROR).send({
          message: 'Could not download the file. ' + err
        });
      }
    });
  }

  private async deleteFile(req: Request, res: Response) {
    const token = req.header('Authorization')?.replace(/Bearer ?/g, '');
    if (token == null) {
      res.status(StatusCodes.UNAUTHORIZED).send();
      return;
    }

    const fileId = decodeURI(req.params.fileId);
    if (fileId == null || fileId === '') {
      res.status(StatusCodes.BAD_REQUEST).send('Invalid search');
    }

    await this.userBusiness.deleteFile(fileId, token);

    // console.log(file);
    res.status(StatusCodes.OK).send();
  }

  // ------------------- SINGLETON --------------------
  private static instance: FileRouter;
  private constructor() {
    super();
  }

  private async init() {
    this.userBusiness = await FileBusiness.getInstance();

    // Manejo de operaciones /api/archivo
    this.router.get('/:fileId', (req, res, next) => {
      this.dowloadFile(req, res).catch(next);
    });

    this.router.delete('/:fileId', (req, res, next) => {
      this.deleteFile(req, res).catch(next);
    });
  }

  static async getInstance() {
    if (FileRouter.instance == null) {
      FileRouter.instance = new FileRouter();
      await FileRouter.instance.init();
    }

    return FileRouter.instance;
  }
}
