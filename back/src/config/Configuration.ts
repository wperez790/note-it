export class Configuration {
  public static UNAUTHORIZED_URL: UrlPermission[] = [
    { url: '/api/login', method: 'POST' },
    { url: '/api/usuario', method: 'POST' },
    { url: '/api/change-password', method: 'POST' },
    { url: '/api/restore-password', method: 'POST' }
    // { url: /\/api\/archivo\/.*/, method: 'GET' }
  ];
}

export interface UrlPermission {
  url: string | RegExp;
  method: 'GET' | 'POST' | 'PUT' | 'DELETE';
}
