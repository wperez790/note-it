import { StatusCodes } from 'http-status-codes';

export class BusinessException extends Error {
  public msg: string;
  public resCode: StatusCodes;

  constructor(resCode: StatusCodes, msg = '') {
    super(msg);
    this.resCode = resCode;
    if (msg != null) {
      this.msg = msg;
    }
  }
}
