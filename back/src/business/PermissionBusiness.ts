import { StatusCodes } from 'http-status-codes';
import { ObjectID } from 'mongodb';
import { BusinessException } from '../Exceptions/BusinessException';
import { Participante, Permission } from '../model/Participant';
import { Utils } from '../service/Utils';

export class PermissionBusiness {
  /**
   * Lanza una excepcion si no tiene permiso
   * @throws BusinessException
   */
  public static hasPermission(
    userId: ObjectID,
    participants: Participante[],
    permission: Permission
  ) {
    if (participants == null) {
      throw new BusinessException(StatusCodes.FORBIDDEN);
    }

    // Obtener mis permisos en la libreta
    const myPermission = Utils.searchObjInArray(
      participants,
      'id',
      userId,
      (e: ObjectID, v: ObjectID) => {
        return e.equals(v);
      }
    ).obj;

    // Si no estoy como participante o no tengo permisos
    if (myPermission == null) {
      throw new BusinessException(StatusCodes.FORBIDDEN);
    }

    // Si tengo el permiso Ej: CREATE
    if (permission !== Permission.VIEW) {
      if (myPermission.permisos == null) {
        throw new BusinessException(StatusCodes.FORBIDDEN);
      }
      const hasPermission = myPermission.permisos.find((elem) => elem === permission) != null;
      if (hasPermission === false) {
        throw new BusinessException(StatusCodes.FORBIDDEN);
      }
    }
  }
}
