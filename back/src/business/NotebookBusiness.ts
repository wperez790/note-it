import { StatusCodes } from 'http-status-codes';
import { ObjectID } from 'mongodb';
import { PermissionBusiness } from '../business/PermissionBusiness';
import { BusinessException } from '../Exceptions/BusinessException';
import { Notebook } from '../model/Notebook';
import { Permission } from '../model/Participant';
import { User } from '../model/User';
import { NotebookRepository } from '../repository/NotebookRepository';
import { UserRepository } from '../repository/UserRepository';
import { NoteBusiness } from './NoteBusiness';
import { UserBusiness } from './UserBusiness';

export class NotebookBusiness {
  private repository: NotebookRepository;
  private userRepository: UserRepository;

  private userBusiness: UserBusiness;
  private noteBusiness: NoteBusiness;

  /**
   * @throws BusinessException
   */
  public async createNotebook(libreta: Notebook, token: string) {
    // Obtener usuario del token
    const user = await this.userBusiness.getUserByToken(token);

    const copyLibreta = new Notebook().copy(libreta, ['titulo', 'descripcion', 'participantes']);
    copyLibreta.id_creador = user._id;

    // Validar la libreta
    await this.validateNotebook(copyLibreta);

    const libretaDB = await this.repository.getNotebookByTitleAndUser(copyLibreta.titulo, user._id);

    if (libretaDB != null) {
      throw new BusinessException(StatusCodes.BAD_REQUEST, 'Notebook already exists');
    }

    const id = await this.repository.createNotebook(copyLibreta);

    if (id == null) {
      throw new BusinessException(StatusCodes.INTERNAL_SERVER_ERROR);
    }

    return id;
  }

  /**
   * @throws BusinessException
   */
  public async updateNotebook(libreta: Notebook, token: string) {
    // Obtener usuario del token
    const user = await this.userBusiness.getUserByToken(token);

    // Tiene id
    if (libreta._id == null) {
      throw new BusinessException(StatusCodes.BAD_REQUEST, '_id field is required');
    }

    // No existe el titulo
    if (libreta.titulo != null) {
      const libretaDB = await this.repository.getNotebookByTitleAndUser(libreta.titulo, user._id);

      if (libretaDB != null) {
        throw new BusinessException(StatusCodes.BAD_REQUEST, 'Notebook already exists');
      }
    }

    const dbNotebook = await this.repository.getNotebookById(new ObjectID(libreta._id));

    if (dbNotebook == null) {
      throw new BusinessException(StatusCodes.NOT_FOUND, 'Notebook not found');
    }

    // Comprobar que seas el creador
    if (!dbNotebook.id_creador.equals(user._id)) {
      throw new BusinessException(StatusCodes.FORBIDDEN);
    }

    // Copiar la libreta
    dbNotebook.copy(libreta, ['titulo', 'descripcion', 'participantes']);

    // Validar libreta
    await this.validateNotebook(dbNotebook);

    const updated = await this.repository.updateNotebook(dbNotebook);
    if (updated == null) {
      throw new BusinessException(StatusCodes.INTERNAL_SERVER_ERROR);
    }
    return updated;
  }

  public async getNotebook(token: string) {
    // Obtener usuario del token
    const user = await this.userBusiness.getUserByToken(token);

    const libretas = await this.repository.getNotebooksByUser(user._id);
    return libretas;
  }

  public async getSharedNotebook(token: string) {
    // Obtener usuario del token
    const user = await this.userBusiness.getUserByToken(token);

    const libretas = await this.repository.getSharedNotebooksByUser(user._id);
    return libretas;
  }

  /**
   * @throws BusinessException
   */
  private async validateNotebook(libreta: Notebook) {
    // Validar Titulo
    if (libreta.titulo == null) {
      throw new BusinessException(StatusCodes.BAD_REQUEST, 'Bad body, Required {titulo}');
    }

    // Validar Descripcion
    if (libreta.descripcion != null) {
      if (typeof libreta.descripcion !== 'string') {
        throw new BusinessException(StatusCodes.BAD_REQUEST, 'Bad description: must be string');
      }

      if (libreta.descripcion === '') {
        throw new BusinessException(StatusCodes.BAD_REQUEST, 'Bad description: it cant be empty');
      }
    }

    // Validar participantes
    if (libreta.participantes != null) {
      if (!Array.isArray(libreta.participantes)) {
        throw new BusinessException(StatusCodes.BAD_REQUEST, 'Participants have to be a list');
      }
      let user: User | null;
      for (const participant of libreta.participantes) {
        if (participant.id == null) {
          throw new BusinessException(
            StatusCodes.BAD_REQUEST,
            'Bad participant ' +
              participant.id +
              '\n' +
              'Required {id: string, permisos?: (CREATE | UPDATE |DELETE)[]}'
          );
        }

        if (libreta.id_creador.equals(participant.id)) {
          throw new BusinessException(StatusCodes.BAD_REQUEST, `You cannot be a participant`);
        }

        // Validar permisos
        const permisos = participant.permisos;
        if (permisos != null) {
          if (!Array.isArray(permisos)) {
            throw new BusinessException(StatusCodes.BAD_REQUEST, 'Permissions have to be a list');
          }
          for (const p of permisos) {
            if (p !== 'CREATE' && p !== 'DELETE' && p !== Permission.UPDATE) {
              throw new BusinessException(
                StatusCodes.BAD_REQUEST,
                'Permission have to be CREATE, UPDATE or DELETE'
              );
            }
          }
        }

        user = await this.userRepository.getUserById(participant.id);
        if (user == null) {
          throw new BusinessException(
            StatusCodes.BAD_REQUEST,
            `participan not found: ${participant.id}`
          );
        }
      }
    }
  }

  /**
   * @throws BusinessException
   */
  public async deleteNotebookById(notebookId: string | ObjectID, token: string) {
    let idNotebookObj: ObjectID;
    try {
      idNotebookObj = new ObjectID(notebookId);
    } catch (e) {
      throw new BusinessException(StatusCodes.BAD_REQUEST, 'Bad note id');
    }

    // Obtener usuario del token
    const user = await this.userBusiness.getUserByToken(token);

    const notebook = await this.repository.getNotebookById(idNotebookObj);
    if (notebook == null) {
      throw new BusinessException(StatusCodes.NOT_FOUND, 'Notebook not found');
    }

    if (!notebook.id_creador.equals(user._id)) {
      throw new BusinessException(StatusCodes.FORBIDDEN);
    }

    await this.noteBusiness.deleteNoteByNotebookId(idNotebookObj, token);
    const deleted = await this.repository.deleteNotebookById(idNotebookObj);

    if (deleted == null) {
      throw new BusinessException(StatusCodes.INTERNAL_SERVER_ERROR);
    }

    if (deleted === 0) {
      throw new BusinessException(StatusCodes.NOT_FOUND);
    }
  }

  /**
   * @throws BusinessException
   */
  public async checkPermissionByNotebookId(
    userId: ObjectID,
    notebookId: ObjectID,
    permission: Permission
  ) {
    const notebook = await this.repository.getNotebookById(notebookId);
    if (notebook == null) {
      throw new BusinessException(
        StatusCodes.NOT_FOUND,
        'Notebook with id ' + notebookId + ' not found'
      );
    }
    if (!userId.equals(notebook.id_creador)) {
      PermissionBusiness.hasPermission(userId, notebook.participantes, permission);
    }
  }

  // ----------------- SINGLETON ---------------------
  private static instance: NotebookBusiness;
  private constructor() {}

  private async init() {
    this.repository = await NotebookRepository.getInstance();
    this.userRepository = await UserRepository.getInstance();
    this.userBusiness = await UserBusiness.getInstance();
    this.noteBusiness = await NoteBusiness.getInstance();
  }

  static async getInstance() {
    if (NotebookBusiness.instance == null) {
      NotebookBusiness.instance = new NotebookBusiness();
      await NotebookBusiness.instance.init();
    }

    return NotebookBusiness.instance;
  }
}
