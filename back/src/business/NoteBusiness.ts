import { StatusCodes } from 'http-status-codes';
import { ObjectID } from 'mongodb';
import { PermissionBusiness } from '../business/PermissionBusiness';
import { BusinessException } from '../Exceptions/BusinessException';
import { Note } from '../model/Note';
import { Notebook } from '../model/Notebook';
import { Permission } from '../model/Participant';
import { User } from '../model/User';
import { NotebookRepository } from '../repository/NotebookRepository';
import { NoteRepository } from '../repository/NoteRepository';
import { UserRepository } from '../repository/UserRepository';
import { Utils } from '../service/Utils';
import { FileBusiness } from './FileBusiness';
import { NotebookBusiness } from './NotebookBusiness';
import { UserBusiness } from './UserBusiness';

export class NoteBusiness {
  private repository: NoteRepository;
  private userRepository: UserRepository;
  private notebookRepository: NotebookRepository;

  private fileBusiness: FileBusiness;
  private userBusiness: UserBusiness;
  private notebookBusiness: NotebookBusiness;

  /**
   * @throws BusinessException
   */
  public async createNote(note: Note, token: string) {
    const user = await this.userBusiness.getUserByToken(token);

    // Validar que tenga titulo
    if (note.titulo == null) {
      throw new BusinessException(StatusCodes.BAD_REQUEST, 'Bad body, Required {titulo}');
    }

    let copyNote: Note;
    try {
      copyNote = new Note().copy(note, [
        'checklist',
        'descripcion',
        'id_libreta',
        'participantes',
        'titulo'
      ]);
    } catch (error) {
      throw new BusinessException(StatusCodes.BAD_REQUEST, 'Invalid notebook id');
    }

    // Validar la nota
    copyNote.id_creador = user._id;
    await this.validateNote(copyNote);

    let notesDB: Note[];
    let notebook: Notebook | null = null;
    if (copyNote.id_libreta != null) {
      // Validar si tiene permisos en la libreta
      notebook = await this.notebookRepository.getNotebookById(copyNote.id_libreta);
      if (notebook == null) {
        throw new BusinessException(StatusCodes.BAD_REQUEST, 'Notebook not exists');
      }

      // Si no es mi libreta, revisar si tengo permiso
      if (!notebook.id_creador.equals(user._id)) {
        const participants = notebook.participantes;
        PermissionBusiness.hasPermission(user._id, participants, Permission.CREATE);
      }

      notesDB = await this.repository.getNotesByTituloAndIdLibreta(
        copyNote.titulo,
        copyNote.id_libreta
      );
    } else {
      notesDB = await this.repository.getNotesByTituloAndIdCreator(copyNote.titulo, user._id);
    }
    if (notesDB.length > 0) {
      throw new BusinessException(StatusCodes.BAD_REQUEST, 'Note already exists');
    }

    const id = await this.repository.createNote(copyNote);

    if (id == null) {
      throw new BusinessException(StatusCodes.INTERNAL_SERVER_ERROR);
    }

    return id;
  }

  /**
   * @throws BusinessException
   */
  public async getOwnNotes(token: string) {
    const user = await this.userBusiness.getUserByToken(token);

    const notes = await this.repository.getNotesByUserAndIdLibreta(user._id, undefined);

    return notes;
  }

  /**
   * @throws BusinessException
   */
  public async getNotebookNotes(idNotebook: string, token: string) {
    const user = await this.userBusiness.getUserByToken(token);

    let idNotebookObj: ObjectID;
    try {
      idNotebookObj = new ObjectID(idNotebook);
    } catch (e) {
      throw new BusinessException(StatusCodes.BAD_REQUEST, 'Bad notebook id');
    }
    const notebook = await this.notebookRepository.getNotebookById(idNotebookObj);
    if (notebook == null) {
      throw new BusinessException(StatusCodes.NOT_FOUND, 'Notebook not found');
    }

    // Preguntar si se tiene permiso si no es el creador
    if (!notebook.id_creador.equals(user._id)) {
      PermissionBusiness.hasPermission(user._id, notebook.participantes, Permission.VIEW);
    }

    const notes = await this.repository.getNotesByNotebookId(notebook._id);

    return notes;
  }

  /**
   * @throws BusinessException
   */
  public async getNoteById(idNote: string, token: string) {
    const user = await this.userBusiness.getUserByToken(token);

    let idNoteObj: ObjectID;
    try {
      idNoteObj = new ObjectID(idNote);
    } catch (e) {
      throw new BusinessException(StatusCodes.BAD_REQUEST, 'Invalid Note ID');
    }

    // Preguntar si se tiene permiso si no es el creador
    await this.checkPermissionByNoteId(user._id, idNoteObj, Permission.VIEW);

    const note = await this.repository.getNoteById(idNoteObj);
    if (note == null) {
      throw new BusinessException(StatusCodes.NOT_FOUND);
    }

    return note;
  }

  /**
   * @throws BusinessException
   */
  public async getSharedNotes(token: string) {
    const user = await this.userBusiness.getUserByToken(token);

    const libretas = await this.repository.getSharedNotesByUser(user._id);
    return libretas;
  }

  /**
   * @throws BusinessException
   */
  public async updateNote(note: Note, token: string) {
    // Validar el token
    const user = await this.userBusiness.getUserByToken(token);

    // Validar si tiene el campo ID
    if (note._id == null) {
      throw new BusinessException(StatusCodes.BAD_REQUEST, 'Field "_id" required');
    }

    // Transformar el objeto recivido a una clase Note
    const updatedNote = new Note().copy(note, [
      '_id',
      'checklist',
      'descripcion',
      'id_libreta',
      'participantes',
      'titulo'
    ]);

    // Validar que exista la nota
    let dbNote: Note | null;
    try {
      dbNote = await this.repository.getNoteById(new ObjectID(note._id));
    } catch (error) {
      throw new BusinessException(StatusCodes.BAD_REQUEST, 'Invalid notebook id');
    }
    if (dbNote == null) {
      throw new BusinessException(StatusCodes.NOT_FOUND, 'Note id not found');
    }

    // Verificar si tengo permiso para editar la nota
    await this.checkPermissionByNoteId(user._id, dbNote._id, Permission.UPDATE);

    // Actualizar la nota
    dbNote.copy(updatedNote);

    // Validar la nota
    await this.validateNote(dbNote);

    let dbNotes: Note[] | null = null;

    // Validar si tiene permisos en la libreta
    if (dbNote.id_libreta != null) {
      if (updatedNote.titulo) {
        dbNotes = await this.repository.getNotesByTituloAndIdLibreta(
          dbNote.titulo,
          dbNote.id_libreta
        );
      }
    } else {
      if (updatedNote.titulo) {
        dbNotes = await this.repository.getNotesByTituloAndIdCreator(dbNote.titulo, user._id);
      }
    }

    // Si existe la nota con ese titulo
    if (dbNotes != null && dbNotes.length > 0) {
      throw new BusinessException(StatusCodes.BAD_REQUEST, 'Note already exists');
    }

    const updated = await this.repository.updateNote(dbNote);

    if (updated == null) {
      throw new BusinessException(StatusCodes.INTERNAL_SERVER_ERROR);
    }

    return updated;
  }

  /**
   * @throws BusinessException
   */
  private async validateNote(note: Note) {
    // Validar Descripcion
    if (note.descripcion != null) {
      if (typeof note.descripcion !== 'string') {
        throw new BusinessException(StatusCodes.BAD_REQUEST, 'Bad description: must be string');
      }

      if (note.descripcion === '') {
        throw new BusinessException(StatusCodes.BAD_REQUEST, 'Bad description: it cant be empty');
      }
    }

    // Validar participantes
    if (note.participantes != null) {
      let user: User | null;
      if (!Array.isArray(note.participantes)) {
        throw new BusinessException(StatusCodes.BAD_REQUEST, 'Participants have to be a list');
      }
      for (const participant of note.participantes) {
        if (participant.id == null) {
          throw new BusinessException(
            StatusCodes.BAD_REQUEST,
            'Bad participant ' +
              participant.id +
              '\n' +
              'Required {id: string, permisos?: (UPDATE | DELETE)[]}'
          );
        }

        // El creador no debe estar como participante
        if (note.id_creador.equals(participant.id)) {
          throw new BusinessException(StatusCodes.BAD_REQUEST, `You cannot be a participant`);
        }

        // El usuario debe existir
        user = await this.userRepository.getUserById(participant.id);
        if (user == null) {
          throw new BusinessException(
            StatusCodes.BAD_REQUEST,
            `participan not found: ${participant.id}`
          );
        }

        // Validar permisos
        const permisos = participant.permisos;
        if (permisos != null) {
          if (!Array.isArray(permisos)) {
            throw new BusinessException(StatusCodes.BAD_REQUEST, 'Permissions have to be a list');
          }
          for (const p of permisos) {
            if (p !== Permission.UPDATE && p !== Permission.DELETE) {
              throw new BusinessException(
                StatusCodes.BAD_REQUEST,
                'Permission have to be UPDATE or DELETE'
              );
            }
          }
        }
      }
    }

    // Validar checklist
    if (note.checklist != null) {
      if (!Array.isArray(note.checklist)) {
        throw new BusinessException(StatusCodes.BAD_REQUEST, 'Field checklist must be Array');
      }
      for (const elem of note.checklist) {
        if (
          elem.desc == null ||
          elem.value == null ||
          typeof elem.desc !== 'string' ||
          typeof elem.value !== 'boolean'
        ) {
          throw new BusinessException(
            StatusCodes.BAD_REQUEST,
            'Element of checklist must be {desc: string, value: boolean}'
          );
        }
      }
    }
  }

  /**
   * @throws BusinessException
   */
  public async deleteNoteById(noteId: string | ObjectID, token: string) {
    let idNoteObj: ObjectID;
    try {
      idNoteObj = new ObjectID(noteId);
    } catch (e) {
      throw new BusinessException(StatusCodes.BAD_REQUEST, 'Bad note id');
    }

    // Comprobar el permiso
    const user = await this.userBusiness.getUserByToken(token);
    await this.checkPermissionByNoteId(user._id, idNoteObj, Permission.DELETE);

    await this.fileBusiness.deleteFilesByNoteId(idNoteObj, token);
    const deleted = await this.repository.deleteNoteById(idNoteObj);

    if (deleted == null) {
      throw new BusinessException(StatusCodes.INTERNAL_SERVER_ERROR);
    }

    if (deleted === 0) {
      throw new BusinessException(StatusCodes.NOT_FOUND);
    }
  }

  /**
   * @throws BusinessException
   */
  public async deleteNoteByNotebookId(notebookId: string | ObjectID, token: string) {
    let idNotebookObj: ObjectID;
    try {
      idNotebookObj = new ObjectID(notebookId);
    } catch (e) {
      throw new BusinessException(StatusCodes.BAD_REQUEST, 'Bad note id');
    }

    // Verificar que tenga permiso
    const user = await this.userBusiness.getUserByToken(token);
    await this.notebookBusiness.checkPermissionByNotebookId(
      user._id,
      idNotebookObj,
      Permission.DELETE
    );

    const notes = await this.repository.getNotesByNotebookId(idNotebookObj);
    for (const note of notes) {
      await this.fileBusiness.deleteFilesByNoteId(note._id, token);
      await this.repository.deleteNoteById(note._id);
    }
  }

  /**
   * @throws BusinessException
   */
  public async getNoteParticipants(noteId: string, token: string) {
    let idNoteObj: ObjectID;
    try {
      idNoteObj = new ObjectID(noteId);
    } catch (e) {
      throw new BusinessException(StatusCodes.BAD_REQUEST, 'Bad note id');
    }

    // Comprobar el permiso
    const user = await this.userBusiness.getUserByToken(token);
    await this.checkPermissionByNoteId(user._id, idNoteObj, Permission.VIEW);

    const note = await this.repository.getNoteById(idNoteObj);
    if (note == null) {
      throw new BusinessException(StatusCodes.NOT_FOUND);
    }
    if (note.participantes == null) {
      return [];
    }
    const idUserList: ObjectID[] = [];
    for (const participant of note.participantes) {
      idUserList.push(participant.id);
    }
    const users = await this.userRepository.getUserInfoByInsideIdList(idUserList);
    if (users == null) {
      return [];
    }
    for (const u of users) {
      const participante = Utils.searchObjInArray(
        note.participantes,
        'id',
        u._id,
        (elem: ObjectID, v: ObjectID) => {
          return elem.equals(v);
        }
      ).obj;
      if (participante != null && participante.permisos != null) {
        u['permisos'] = participante.permisos;
      }
    }
    return users;
  }

  /**
   * @throws BusinessException
   */
  public async checkPermissionByNoteId(userId: ObjectID, noteId: ObjectID, permission: Permission) {
    const note = await this.repository.getNoteById(noteId);
    if (note == null) {
      throw new BusinessException(StatusCodes.NOT_FOUND, 'note with id ' + noteId + ' not found');
    }

    if (note.id_libreta != null) {
      try {
        PermissionBusiness.hasPermission(userId, note.participantes, permission);
      } catch (e) {
        await this.notebookBusiness.checkPermissionByNotebookId(
          userId,
          note.id_libreta,
          permission
        );
      }
    } else {
      // Verificar si tengo permiso para editar la nota
      if (!note.id_creador.equals(userId)) {
        const participants = note.participantes;
        PermissionBusiness.hasPermission(userId, participants, permission);
      }
    }
  }

  // ----------------- SINGLETON ---------------------
  private static instance: NoteBusiness;
  private constructor() {}

  private async init() {
    this.repository = await NoteRepository.getInstance();
    this.userRepository = await UserRepository.getInstance();
    this.notebookRepository = await NotebookRepository.getInstance();
    this.userBusiness = await UserBusiness.getInstance();
    this.fileBusiness = await FileBusiness.getInstance();
    this.notebookBusiness = await NotebookBusiness.getInstance();
  }

  static async getInstance() {
    if (NoteBusiness.instance == null) {
      NoteBusiness.instance = new NoteBusiness();
      await NoteBusiness.instance.init();
    }

    return NoteBusiness.instance;
  }
}
