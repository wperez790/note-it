import { StatusCodes } from 'http-status-codes';
import { ObjectID } from 'mongodb';
import { BusinessException } from '../Exceptions/BusinessException';
import { User } from '../model/User';
import { ValidatorRegex } from '../model/ValidatorRegex';
import { UserRepository } from '../repository/UserRepository';
import { JWT } from '../service/jwt/Jwt';
import { Email } from '../service/mail/Email';
import { PasswordEncoder } from '../service/passwordEncoder/PasswordEncoder';
import { Utils } from '../service/Utils';

export class UserBusiness {
  private emailService = Email.getInstance();
  private repository: UserRepository;

  /**
   * @throws BusinessException
   */
  public async login(user: User) {
    if (user.email == null || user.password == null) {
      throw new BusinessException(StatusCodes.BAD_REQUEST, 'Bad body -> {email, password}');
    }

    const userDb = await this.repository.getUserByEmail(user.email);
    if (userDb == null) {
      return null;
    }

    const validPassword = PasswordEncoder.comparePassword(user.password, userDb.password);
    if (validPassword === false) {
      return null;
    }

    if (userDb.emailCheck === false) {
      if (user.verificationCode == null) {
        throw new BusinessException(StatusCodes.BAD_REQUEST, 'Email validation required');
      }

      if (user.verificationCode === userDb.verificationCode) {
        userDb.verificationCode = '';
        userDb.emailCheck = true;
        await this.repository.updateUser(userDb);
      } else {
        throw new BusinessException(StatusCodes.BAD_REQUEST, 'Email validation incorrect');
      }
    }

    const token = JWT.generateJWT({ email: userDb.email });
    return token;
  }

  /**
   * @throws BusinessException
   */
  public async createUser(user: User) {
    if (user.password == null || user.email == null || user.surname == null || user.name == null) {
      throw new BusinessException(
        StatusCodes.BAD_REQUEST,
        'Bad body, Required {name, surname, password, email}'
      );
    }

    // Validar el usuario
    this.validateUser(user);

    const userDB = await this.repository.getUserByEmail(user.email);
    if (userDB != null) {
      throw new BusinessException(StatusCodes.BAD_REQUEST, 'Email already in use');
    }

    const copyUser = new User().copy(user, ['name', 'surname', 'password', 'email']);

    copyUser.password = PasswordEncoder.hashPassword(copyUser.password);
    copyUser.verificationCode = Utils.getRandomHash(10);
    const id = await this.repository.createUser(copyUser);

    if (id == null) {
      throw new BusinessException(StatusCodes.INTERNAL_SERVER_ERROR);
    }

    this.emailService.sendMailHTML(
      copyUser.email,
      'Note It Validation\n',
      '<html><body>' +
        '<h2>Welcome to note it</h2>' +
        '<p>To start using the platform, an email validation is required,' +
        ' for this the following code must be entered into the application:<p>' +
        `<br> <b style="font-size: 25px;">${copyUser.verificationCode}</b>` +
        '</body></html>'
    );

    return id;
  }

  /**
   * @throws BusinessException
   */
  public async updateUser(user: User, token: string) {
    if (user.password == null && user.email == null && user.name == null && user.surname == null) {
      throw new BusinessException(
        StatusCodes.BAD_REQUEST,
        'Bad body, Required name, surname, password or email'
      );
    }

    // Validar el usuario
    this.validateUser(user);

    const email = JWT.decode(token)?.email;
    if (email == null) {
      throw new BusinessException(StatusCodes.UNAUTHORIZED);
    }

    const userDB = await this.repository.getUserByEmail(email);
    if (userDB == null) {
      throw new BusinessException(StatusCodes.NOT_FOUND);
    }

    if (user.email != null) {
      const userExists = await this.repository.getUserByEmail(user.email);
      if (userExists != null) {
        throw new BusinessException(StatusCodes.BAD_REQUEST, 'Email in use');
      }
    }

    userDB.copy(user, ['name', 'surname', 'email']);

    if (user.password != null) {
      userDB.password = PasswordEncoder.hashPassword(user.password);
    }

    const id = await this.repository.updateUser(userDB);
    if (id == null) {
      throw new BusinessException(StatusCodes.INTERNAL_SERVER_ERROR);
    }

    return JWT.generateJWT({ email: userDB.email });
  }

  /**
   * @throws BusinessException
   */
  public async requestRestorePassword(user: User) {
    if (user.email == null) {
      throw new BusinessException(StatusCodes.BAD_REQUEST, 'Bad body, Required {email}');
    }

    // Validar el correo
    this.validateUser(user);

    const userDB = await this.repository.getUserByEmail(user.email);
    if (userDB == null) {
      throw new BusinessException(StatusCodes.NOT_FOUND, 'email not found');
    }

    let codeIsUsed = true;
    let code;
    while (codeIsUsed === true) {
      code = Utils.getRandomHash(16);
      codeIsUsed = await this.repository.isVerificationCodeUser(code);
    }
    userDB.verificationCode = code;

    const updated = await this.repository.updateUser(userDB);

    if (updated == null || updated === false) {
      throw new BusinessException(StatusCodes.INTERNAL_SERVER_ERROR);
    }

    this.emailService.sendMailHTML(
      userDB.email,
      'Note It Password Recover\n',
      '<html><body>' +
        '<h2>Password Recover</h2>' +
        '<p>To restore the password, enter the following verification code in the application:<p>' +
        `<br> <b style="font-size: 25px;">${userDB.verificationCode}</b>` +
        '</body></html>'
    );
  }

  /**
   * @throws BusinessException
   */
  public async restorePassword(user: User) {
    if (user.password == null || user.verificationCode == null) {
      throw new BusinessException(
        StatusCodes.BAD_REQUEST,
        'Bad body, Required {password, verificationCode}'
      );
    }

    // Validar la contraseña
    this.validateUser(user);

    const userDB = await this.repository.getUserByVerificationCode(user.verificationCode);
    if (userDB == null) {
      throw new BusinessException(StatusCodes.BAD_REQUEST, 'Invalid verification code');
    }

    userDB.password = PasswordEncoder.hashPassword(user.password);
    userDB.verificationCode = '';

    const updated = await this.repository.updateUser(userDB);
    if (updated == null || updated === false) {
      throw new BusinessException(StatusCodes.INTERNAL_SERVER_ERROR);
    }
  }

  /**
   * @throws BusinessException
   */
  public async findUser(searchParam: string, token: string) {
    // Obtener usuario del token
    const userDB = await this.getUserByToken(token);

    const user = await this.repository.getUserByEmailOrNameOrSurname(searchParam, userDB._id);
    if (user == null) {
      throw new BusinessException(StatusCodes.NOT_FOUND);
    }
    return user;
  }

  private validateUser(user: User) {
    if (user.password != null && user.password.match(ValidatorRegex.PASSWORD) == null) {
      throw new BusinessException(
        StatusCodes.BAD_REQUEST,
        'Invalid password' +
          '\n* Solo letras, números y .' +
          '\n* Se debe comenzar con letras' +
          '\n* Cantidad de caracteres 4 a 20'
      );
    }

    if (user.email != null && user.email.match(ValidatorRegex.EMAIL) == null) {
      throw new BusinessException(StatusCodes.BAD_REQUEST, 'Invalid email');
    }

    if (user.name != null && user.name.match(ValidatorRegex.NAME) == null) {
      throw new BusinessException(StatusCodes.BAD_REQUEST, 'Invalid name');
    }

    if (user.surname != null && user.surname.match(ValidatorRegex.NAME) == null) {
      throw new BusinessException(StatusCodes.BAD_REQUEST, 'Invalid surname');
    }
  }

  /**
   * @throws BusinessException
   */
  public async getUserByToken(token: string) {
    const email = JWT.decode(token)?.email;
    if (email == null) {
      throw new BusinessException(StatusCodes.UNAUTHORIZED);
    }

    const user = await this.repository.getUserByEmail(email);
    if (user == null) {
      throw new BusinessException(StatusCodes.UNAUTHORIZED);
    }
    return user;
  }

  /**
   * @throws BusinessException
   */
  public async getUserById(userId: string, token: string) {
    const email = JWT.decode(token)?.email;
    if (email == null) {
      throw new BusinessException(StatusCodes.UNAUTHORIZED);
    }

    let userIdObj: ObjectID;
    try {
      userIdObj = new ObjectID(userId);
    } catch (e) {
      throw new BusinessException(StatusCodes.BAD_REQUEST, 'Invalid user ID');
    }

    const user = await this.repository.getUserInfoById(userIdObj);
    if (user == null) {
      throw new BusinessException(StatusCodes.NOT_FOUND);
    }
    return user;
  }

  /**
   * @throws BusinessException
   */
  public async getUserInformationByToken(token: string) {
    const email = JWT.decode(token)?.email;
    if (email == null) {
      throw new BusinessException(StatusCodes.UNAUTHORIZED);
    }

    const user = await this.repository.getUserInformationByEmail(email);
    if (user == null) {
      throw new BusinessException(StatusCodes.UNAUTHORIZED);
    }
    return user;
  }

  // ------------------- SINGLETON --------------------
  private static instance: UserBusiness;
  private constructor() {}

  private async init() {
    this.repository = await UserRepository.getInstance();
  }

  static async getInstance() {
    if (UserBusiness.instance == null) {
      UserBusiness.instance = new UserBusiness();
      await UserBusiness.instance.init();
    }

    return UserBusiness.instance;
  }
}
