import * as fs from 'fs';
import { StatusCodes } from 'http-status-codes';
import { ObjectID } from 'mongodb';
import { BusinessException } from '../Exceptions/BusinessException';
import { File } from '../model/File';
import { Permission } from '../model/Participant';
import { FileRepository } from '../repository/FileRepository';
import { NoteRepository } from '../repository/NoteRepository';
import { Utils } from '../service/Utils';
import { NoteBusiness } from './NoteBusiness';
import { UserBusiness } from './UserBusiness';
export class FileBusiness {
  private repository: FileRepository;
  private noteRepository: NoteRepository;

  private userBusiness: UserBusiness;
  private noteBusiness: NoteBusiness;

  public async uploadFileToNote(expressFile, noteId: string, token: string) {
    const user = await this.userBusiness.getUserByToken(token);

    let noteIdObj;
    try {
      noteIdObj = new ObjectID(noteId);
    } catch (e) {
      throw new BusinessException(StatusCodes.BAD_REQUEST, 'Invalid Note Id');
    }

    const note = await this.noteRepository.getNoteById(noteIdObj);
    if (note == null) {
      throw new BusinessException(StatusCodes.NOT_FOUND, 'Note not found');
    }

    // Preguntar si tengo permisos en la nota o en la libreta
    await this.noteBusiness.checkPermissionByNoteId(user._id, noteIdObj, Permission.UPDATE);

    let nameIsUsed = true;
    let path;
    while (nameIsUsed === true) {
      path = './upload/' + Utils.getRandomHash(16);
      nameIsUsed = (await this.repository.getFileByPath(path)) != null;
    }

    const fileMetadata = new File();
    fileMetadata.note_id = noteIdObj;
    fileMetadata.nombre = expressFile.name;
    fileMetadata.mimeType = expressFile.mimetype;
    fileMetadata.path = path;
    fileMetadata.tamaño = expressFile.size;
    fileMetadata.creator_id = user._id;
    const id = await this.repository.createFile(fileMetadata);
    if (id == null) {
      throw new BusinessException(StatusCodes.INTERNAL_SERVER_ERROR);
    }

    expressFile.mv(path);

    return id;
  }

  public async getFilesOfNote(noteId: string, token: string) {
    let noteIdObj;
    try {
      noteIdObj = new ObjectID(noteId);
    } catch (e) {
      throw new BusinessException(StatusCodes.BAD_REQUEST, 'Invalid Note Id');
    }

    const user = await this.userBusiness.getUserByToken(token);

    const note = await this.noteRepository.getNoteById(noteIdObj);
    if (note == null) {
      throw new BusinessException(StatusCodes.NOT_FOUND, 'Note not found');
    }

    // Preguntar si tengo permisos en la nota o en la libreta
    await this.noteBusiness.checkPermissionByNoteId(user._id, noteIdObj, Permission.VIEW);

    const files = this.repository.getFileByNoteId(noteIdObj);

    return files;
  }

  public async getFile(idFile: string, token: string) {
    const user = await this.userBusiness.getUserByToken(token);

    let fileId;
    try {
      fileId = new ObjectID(idFile);
    } catch (e) {
      throw new BusinessException(StatusCodes.BAD_REQUEST, 'Invalid File Id');
    }

    const file = await this.repository.getFileById(fileId);
    if (file == null) {
      throw new BusinessException(StatusCodes.NOT_FOUND, 'File not found');
    }

    // Comprobar permisos
    if (file.note_id != null) {
      await this.noteBusiness.checkPermissionByNoteId(user._id, file.note_id, Permission.VIEW);
    }

    return file;
  }

  /**
   * @throws BusinessException
   */
  public async deleteFilesByNoteId(noteId: string | ObjectID, token: string) {
    let idNoteObj: ObjectID;
    try {
      idNoteObj = new ObjectID(noteId);
    } catch (e) {
      throw new BusinessException(StatusCodes.BAD_REQUEST, 'Bad note id');
    }

    // Comprobar permisos
    const user = await this.userBusiness.getUserByToken(token);
    await this.noteBusiness.checkPermissionByNoteId(user._id, idNoteObj, Permission.DELETE);

    const files = await this.repository.getFileByNoteIdWithPath(idNoteObj);
    for (const file of files) {
      fs.unlinkSync(file.path);
      await this.repository.deleteFileById(file._id);
    }
  }

  public async deleteFile(fileId: string | ObjectID, token: string) {
    let fileIdObj: ObjectID;
    try {
      fileIdObj = new ObjectID(fileId);
    } catch (e) {
      throw new BusinessException(StatusCodes.BAD_REQUEST, 'Bad file id');
    }

    // Comprobar permisos
    const user = await this.userBusiness.getUserByToken(token);
    const file = await this.repository.getFileById(fileIdObj);

    if (file == null) {
      throw new BusinessException(StatusCodes.NOT_FOUND);
    }

    // Comprobar permisos
    if (file.note_id != null) {
      await this.noteBusiness.checkPermissionByNoteId(user._id, file.note_id, Permission.UPDATE);
    } else {
      if (!user._id.equals(file.creator_id)) {
        throw new BusinessException(StatusCodes.FORBIDDEN);
      }
    }

    fs.unlinkSync(file.path);
    const deleted = await this.repository.deleteFileById(fileIdObj);

    if (deleted == null) {
      throw new BusinessException(StatusCodes.INTERNAL_SERVER_ERROR);
    }
  }

  // ----------------- SINGLETON ---------------------
  private static instance: FileBusiness;
  private constructor() {}

  private async init() {
    this.repository = await FileRepository.getInstance();
    this.userBusiness = await UserBusiness.getInstance();
    this.noteRepository = await NoteRepository.getInstance();
    this.noteBusiness = await NoteBusiness.getInstance();
  }

  static async getInstance() {
    if (FileBusiness.instance == null) {
      FileBusiness.instance = new FileBusiness();
      await FileBusiness.instance.init();
    }

    return FileBusiness.instance;
  }
}
