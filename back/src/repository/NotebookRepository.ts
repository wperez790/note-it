import { FilterQuery, ObjectID } from 'mongodb';
import { Notebook } from '../model/Notebook';
import { MongoConnection } from './MongoConnection';

type QueryType = FilterQuery<Notebook>;
export class NotebookRepository {
  private db: MongoConnection;
  private COLLECTION = 'libreta';

  public async createNotebook(libreta: Notebook) {
    return await this.db.insert(this.COLLECTION, libreta);
  }

  public async getNotebookById(id: ObjectID) {
    const query: QueryType = {
      _id: id
    };

    const res = await this.db.find<Notebook>(this.COLLECTION, query);
    if (res[0] == null) {
      return null;
    }
    return new Notebook().copy(res[0]);
  }

  public async getNotebooksByUser(id_creador: ObjectID) {
    const query: QueryType = {
      id_creador: id_creador
    };
    const out: Notebook[] = [];

    const res = await this.db.find(this.COLLECTION, query);
    if (res == null) {
      return [];
    }
    for (const elem of res) {
      out.push(
        new Notebook().copy(elem, ['_id', 'titulo', 'descripcion', 'participantes', 'id_creador'])
      );
    }

    return out;
  }

  public async getSharedNotebooksByUser(id_creador: ObjectID) {
    const query: QueryType = {
      'participantes.id': id_creador
    };

    const res = await this.db.find(this.COLLECTION, query);

    const out: Notebook[] = [];
    if (res == null) {
      return [];
    }
    for (const elem of res) {
      out.push(
        new Notebook().copy(elem, ['_id', 'titulo', 'descripcion', 'participantes', 'id_creador'])
      );
    }

    return out;
  }

  public async getNotebookByTitleAndUser(title: string, id_creator: ObjectID) {
    const query: QueryType = {
      titulo: title,
      id_creador: id_creator
    };

    const res = await this.db.find<Notebook>(this.COLLECTION, query);
    if (res[0] == null) {
      return null;
    }
    return new Notebook().copy(res[0]);
  }

  public async updateNotebook(notebook: Notebook) {
    return await this.db.update(this.COLLECTION, notebook);
  }

  public async deleteNotebookById(_id: ObjectID) {
    const query: QueryType = {
      _id: _id
    };

    return await this.db.delete(this.COLLECTION, query);
  }

  // ------------------- SINGLETON --------------------
  private static instance: NotebookRepository;
  private constructor() {}

  private async init() {
    this.db = await MongoConnection.getInstance();
  }

  static async getInstance() {
    if (NotebookRepository.instance == null) {
      NotebookRepository.instance = new NotebookRepository();
      await NotebookRepository.instance.init();
    }

    return NotebookRepository.instance;
  }
}
