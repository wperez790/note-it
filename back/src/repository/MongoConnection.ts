// import * as Mongodb from 'mongodb';

import { connect, Db, FilterQuery } from 'mongodb';

export class MongoConnection {
  private connection: Db;

  /**
   *
   * @param collection
   * @param query
   * @param proyection Que campos traer
   */
  public async find<T>(
    collection: string,
    query: FilterQuery<any> = {},
    proyection?: any
  ): Promise<T[]> {
    return await this.connection
      .collection(collection)
      .find(query, { projection: proyection })
      .toArray();
  }

  public async insert<T>(collection: string, element: T) {
    const res = await this.connection.collection(collection).insertOne(element);
    return res.insertedId;
  }

  public async update<T>(collection: string, element: T) {
    const id = element['_id'];
    if (id == null) {
      return null;
    }
    const res = await this.connection
      .collection(collection)
      .updateOne({ _id: id }, { $set: element });
    return res.modifiedCount > 0;
  }

  public async delete(collection: string, query: FilterQuery<any> = {}) {
    const res = await this.connection.collection(collection).deleteMany(query);
    return res.deletedCount;
  }

  // ------------------- SINGLETON --------------------
  private static instance: MongoConnection;
  private constructor() {}

  private async init() {
    const uri = 'mongodb://mongodb:27017/pf-api';
    const res = await connect(uri, { useUnifiedTopology: true });
    this.connection = res.db('pf-api');
  }

  static async getInstance() {
    if (MongoConnection.instance == null) {
      MongoConnection.instance = new MongoConnection();
      await MongoConnection.instance.init();
    }

    return MongoConnection.instance;
  }
}
