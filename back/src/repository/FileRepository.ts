import { FilterQuery, ObjectID } from 'mongodb';
import { File } from '../model/File';
import { MongoConnection } from './MongoConnection';

type QueryType = FilterQuery<File>;
export class FileRepository {
  private db: MongoConnection;
  private COLLECTION = 'file';

  public async createFile(file: File) {
    return await this.db.insert(this.COLLECTION, file);
  }

  public async getFileByPath(path: string) {
    const query: QueryType = { path: path };

    const res = await this.db.find(this.COLLECTION, query);
    if (res[0] == null) {
      return null;
    }
    return new File().copy(res[0]);
  }

  public async getFileById(id: ObjectID) {
    const query: QueryType = { _id: id };

    const res = await this.db.find(this.COLLECTION, query);
    if (res[0] == null) {
      return null;
    }
    return new File().copy(res[0]);
  }

  public async getFileByNoteId(noteId: ObjectID) {
    const query: QueryType = { note_id: noteId };

    const res = await this.db.find(this.COLLECTION, query);
    const out: File[] = [];
    if (res == null) {
      return [];
    }
    for (const elem of res) {
      out.push(new File().copy(elem, ['_id', 'nombre', 'tamaño', 'mimeType', 'creator_id']));
    }
    return out;
  }

  public async getFileByNoteIdWithPath(noteId: ObjectID) {
    const query: QueryType = { note_id: noteId };

    const res = await this.db.find(this.COLLECTION, query);
    const out: File[] = [];
    if (res == null) {
      return [];
    }
    for (const elem of res) {
      out.push(
        new File().copy(elem, ['_id', 'nombre', 'tamaño', 'mimeType', 'creator_id', 'path'])
      );
    }
    return out;
  }

  public async deleteFileById(_id: ObjectID) {
    const query: QueryType = {
      _id: _id
    };

    return await this.db.delete(this.COLLECTION, query);
  }

  /**
   *
   * @param note_id
   * @returns Cantidad de elementos borrados
   */
  public async deleteFileByNoteId(note_id: ObjectID) {
    const query: QueryType = {
      note_id: note_id
    };

    return await this.db.delete(this.COLLECTION, query);
  }

  // ------------------- SINGLETON --------------------
  private static instance: FileRepository;
  private constructor() {}

  private async init() {
    this.db = await MongoConnection.getInstance();
  }

  static async getInstance() {
    if (FileRepository.instance == null) {
      FileRepository.instance = new FileRepository();
      await FileRepository.instance.init();
    }

    return FileRepository.instance;
  }
}
