import { FilterQuery, ObjectID } from 'mongodb';
import { Note } from '../model/Note';
import { MongoConnection } from './MongoConnection';

type QueryType = FilterQuery<Note>;

export class NoteRepository {
  private db: MongoConnection;
  private COLLECTION = 'nota';

  public async createNote(note: Note) {
    return await this.db.insert(this.COLLECTION, note);
  }

  public async updateNote(note: Note) {
    return await this.db.update(this.COLLECTION, note);
  }

  public async getNotesByTituloAndIdLibreta(titulo: string, id_libreta: ObjectID) {
    const query: QueryType = {
      titulo: titulo,
      id_libreta: id_libreta
    };
    const out: Note[] = [];

    const res = await this.db.find(this.COLLECTION, query);
    if (res == null) {
      return [];
    }
    for (const elem of res) {
      out.push(new Note().copy(elem));
    }

    return out;
  }

  public async getNotesByParticipantsId(titulo: string, id_libreta: ObjectID) {
    const query: QueryType = {
      titulo: titulo,
      id_libreta: id_libreta
    };
    const out: Note[] = [];

    const res = await this.db.find(this.COLLECTION, query);
    if (res == null) {
      return [];
    }
    for (const elem of res) {
      out.push(new Note().copy(elem));
    }

    return out;
  }

  public async getNotesByNotebookId(id_libreta: ObjectID) {
    const query: QueryType = {
      id_libreta: id_libreta
    };
    const out: Note[] = [];

    const res = await this.db.find(this.COLLECTION, query);
    if (res == null) {
      return [];
    }
    for (const elem of res) {
      out.push(new Note().copy(elem));
    }

    return out;
  }

  /**
   *
   * @param _id
   * @returns Cantidad de elementos borrados
   */
  public async deleteNoteById(_id: ObjectID) {
    const query: QueryType = {
      _id: _id
    };

    return await this.db.delete(this.COLLECTION, query);
  }

  /**
   *
   * @param id_libreta
   * @returns Cantidad de elementos borrados
   */
  public async deleteNoteByNotebookId(id_libreta: ObjectID) {
    const query: QueryType = {
      id_libreta: id_libreta
    };

    return await this.db.delete(this.COLLECTION, query);
  }

  public async getNotesByTituloAndIdCreator(titulo: string, id_creador: ObjectID) {
    const query: QueryType = {
      titulo: titulo,
      id_creador: id_creador,
      id_libreta: undefined
    };
    const out: Note[] = [];

    const res = await this.db.find(this.COLLECTION, query);
    if (res == null) {
      return [];
    }
    for (const elem of res) {
      out.push(new Note().copy(elem));
    }

    return out;
  }

  public async getNotesByUserAndIdLibreta(id_creador: ObjectID, id_libreta: ObjectID | undefined) {
    const query: QueryType = {
      id_creador: id_creador,
      id_libreta: id_libreta
    };
    const out: Note[] = [];

    const res = await this.db.find(this.COLLECTION, query);
    if (res == null) {
      return [];
    }
    for (const elem of res) {
      out.push(new Note().copy(elem));
    }

    return out;
  }

  public async getNoteById(id: ObjectID) {
    const query: QueryType = {
      _id: id
    };

    const res = await this.db.find(this.COLLECTION, query);
    if (res[0] == null) {
      return null;
    }
    return new Note().copy(res[0]);
  }

  public async getSharedNotesByUser(id_creador: ObjectID) {
    const query: QueryType = {
      'participantes.id': id_creador,
      id_libreta: undefined
    };
    const out: Note[] = [];

    const res = await this.db.find(this.COLLECTION, query);
    if (res == null) {
      return [];
    }
    for (const elem of res) {
      out.push(new Note().copy(elem));
    }

    return out;
  }

  // ------------------- SINGLETON --------------------
  private static instance: NoteRepository;
  private constructor() {}

  private async init() {
    this.db = await MongoConnection.getInstance();
  }

  static async getInstance() {
    if (NoteRepository.instance == null) {
      NoteRepository.instance = new NoteRepository();
      await NoteRepository.instance.init();
    }

    return NoteRepository.instance;
  }
}
