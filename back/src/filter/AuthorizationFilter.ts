import { Request, Response } from 'express';
import { StatusCodes } from 'http-status-codes';
import { Configuration } from '../config/Configuration';
import { Utils } from '../service/Utils';

export function AuthorizationFilter(request: Request, response: Response, next: any) {
  const permission = Utils.searchObjInArray(
    Configuration.UNAUTHORIZED_URL,
    'url',
    request.url,
    (e: string | RegExp, v: string) => {
      return v.match(new RegExp(e)) != null;
    }
  ).obj;
  if (request.method === 'OPTIONS') {
    next();
    return;
  }
  if (permission != null && permission.method === request.method) {
    next();
    return;
  }

  const tokenHeader = request.headers.authorization;
  if (tokenHeader == null) {
    response.statusCode = StatusCodes.UNAUTHORIZED;
    response.send();
    return;
  }

  next();
}
