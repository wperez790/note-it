import { ObjectID } from 'mongodb';
import { Utils } from '../service/Utils';

export class File {
  public _id: ObjectID;
  public note_id?: ObjectID;
  public creator_id: ObjectID;
  public nombre: string;
  public mimeType: string;
  public path: string;
  public tamaño: number;

  public copy(obj: any, keys?: (keyof File)[]) {
    if (obj == null) {
      return this;
    }

    if (keys != null) {
      Utils.copyToObj(this, keys, obj);
    } else {
      Utils.copyToObj(this, File.Keys, obj);
    }

    if (this._id != null) {
      this._id = new ObjectID(this._id);
    }

    if (this.note_id != null) {
      this.note_id = new ObjectID(this.note_id);
    }

    if (this.creator_id != null) {
      this.creator_id = new ObjectID(this.creator_id);
    }

    return this;
  }
}

// public _id?: ObjectID;
// public note_id?: ObjectID;
// public creator_id: ObjectID;
// public nombre: string;
// public mimeType: string;
// public path: string;
// public tamaño: number;
export namespace File {
  export enum Keys {
    ID = '_id',
    NOTE_ID = 'note_id',
    CREATOR_ID = 'creator_id',
    NOMBRE = 'nombre',
    MIMETYPE = 'mimeType',
    PATH = 'path',
    TAMAÑO = 'tamaño'
  }
}
