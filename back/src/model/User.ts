import { ObjectID } from 'mongodb';
import { Utils } from '../service/Utils';

export class User {
  public _id: ObjectID;
  public password: string;
  public name: string;
  public surname: string;
  public email: string;
  public emailCheck? = false;
  public verificationCode?: string;

  public copy(obj: any, keys?: (keyof User)[]) {
    if (obj == null) {
      return this;
    }

    if (keys != null) {
      Utils.copyToObj(this, keys, obj);
    } else {
      Utils.copyToObj(this, User.Keys, obj);
    }

    if (this._id != null) {
      this._id = new ObjectID(this._id);
    }

    return this;
  }
}

export namespace User {
  export enum Keys {
    ID = '_id',
    PASSWORD = 'password',
    EMAIL = 'email',
    EMAIL_CHECK = 'emailCheck',
    VERIFICATION_CODE = 'verificationCode',
    NAME = 'name',
    SURNANE = 'surname'
  }
}
