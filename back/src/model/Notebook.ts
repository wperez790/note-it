import { ObjectID } from 'mongodb';
import { Utils } from '../service/Utils';
import { Participante } from './Participant';

export class Notebook {
  public _id: ObjectID;
  public id_creador: ObjectID;
  public titulo: string;
  public descripcion?: string;
  public participantes: Participante[];

  public copy(obj: any, keys?: (keyof Notebook)[]) {
    if (obj == null) {
      return this;
    }

    if (keys != null) {
      Utils.copyToObj(this, keys, obj);
    } else {
      Utils.copyToObj(this, Notebook.Keys, obj);
    }

    // transformar el string a ObjectID
    if (this._id != null) {
      this._id = new ObjectID(this._id);
    }

    if (this.id_creador != null) {
      this.id_creador = new ObjectID(this.id_creador);
    }

    // transformando de any[] a Participante[]
    if (this.participantes != null) {
      for (let i = 0; i < this.participantes.length; i++) {
        this.participantes[i] = new Participante().copy(this.participantes[i]);
      }
    }

    return this;
  }
}

export namespace Notebook {
  export enum Keys {
    ID = '_id',
    ID_CREADOR = 'id_creador',
    TITULO = 'titulo',
    DESCRIPCION = 'descripcion',
    PARTICIPANTES = 'participantes'
  }
}
