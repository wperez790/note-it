import { Router } from 'express';

export class ApiRouter {
  public router = Router();

  getRouter() {
    return this.router;
  }
}
