import { ObjectID } from 'mongodb';
import { Utils } from '../service/Utils';
import { Participante } from './Participant';

export class Note {
  public _id: ObjectID;
  public id_creador: ObjectID;
  public id_libreta: ObjectID;
  public titulo: string;
  public descripcion?: string;
  public participantes: Participante[];
  public fecha_creacion: number;
  public fecha_ultima_actualizacion: number;
  public checklist: { desc: string; value: boolean }[];

  public copy(obj: any, keys?: (keyof Note)[]) {
    if (obj == null) {
      return this;
    }

    if (keys != null) {
      Utils.copyToObj(this, keys, obj);
    } else {
      Utils.copyToObj(this, Note.Keys, obj);
    }

    // transformar el string a ObjectID
    if (this._id != null) {
      this._id = new ObjectID(this._id);
    }

    if (this.id_creador != null) {
      this.id_creador = new ObjectID(this.id_creador);
    }

    if (this.id_libreta != null) {
      this.id_libreta = new ObjectID(this.id_libreta);
    }

    // transformando de any[] a Participante[]
    if (this.participantes != null) {
      if (Array.isArray(this.participantes)) {
        for (let i = 0; i < this.participantes.length; i++) {
          this.participantes[i] = new Participante().copy(this.participantes[i]);
        }
      } else {
        this.participantes = [];
      }
    }

    // Copiando los valores exactos de checklist
    if (this.checklist != null) {
      if (Array.isArray(this.checklist)) {
        for (let i = 0; i < this.checklist.length; i++) {
          this.checklist[i] = { desc: this.checklist[i].desc, value: this.checklist[i].value };
        }
      } else {
        this.checklist = [];
      }
    }

    return this;
  }
}

export namespace Note {
  export enum Keys {
    ID = '_id',
    ID_CREADOR = 'id_creador',
    ID_LIBRETA = 'id_libreta',
    PARTICIPANTS = 'participantes',
    TITULO = 'titulo',
    DESCRIPCION = 'descripcion',
    FECHA_CREACION = 'fecha_creacion',
    FECHA_ULTIMA_ACTUALIZACION = 'fecha_ultima_actualizacion',
    CHECKLIST = 'checklist'
  }
}
