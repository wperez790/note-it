# Note It: Módulo de Node-RED
---

Este repositorio contiene la implementación del modulo de Node-RED, con los distintos nodos que consumen la API Rest en Node.js hecha en el módulo de Backend (carpeta **_back/_**)

## Arquitectura
---

![Imagen Arquitectura](https://svgshare.com/i/RmH.svg)

## Funciones del módulo
---

- Proveer un conjunto de **nodos** que consuman la **API REST** de Note-It.
- Disponer la posibilidad de utilizar los **nodos** en forma **gráfica** mediante un **dashboard**

## Nodos implementados
---

### Login

- Login ☑
- Recuperar contraseña ☑
- Cambiar contraseña ☑

### Usuario

- Crear usuario ☑
- Actualizar usuario ☑
- Obtener usuario actual ☑
- Obtener usuarios (por algún criterio de búsqueda) ☑

### Libreta

- Crear libreta ☑
- Actualizar libreta ☑
- Obtener libretas ☑
- Obtener libretas compartidas ☑
- Eliminar libreta ☐ **TODO**

### Nota

- Crear nota ☑
- Actualizar nota ☑
- Obtener notas sin agrupar ☑
- Obtener notas compartidas sin agrupar ☑
- Obtener notas de una libreta ☑
- Eliminar nota ☐ **TODO**

### Archivo

- Subir archivo a nota ☐ **TODO**
- Obtener archivos de una nota ☑
- Descargar archivo ☐ **TODO**

## Pasos para ejecutar el modulo
---

- Para interactuar con los diferentes nodos en el **entorno de Node-RED**: `https://pf2020.ddns.net/`

- Para interactuar con los diferentes nodos en un **dashboard de Node-RED**: `https://pf2020.ddns.net/ui`