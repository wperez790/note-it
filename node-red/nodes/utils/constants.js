const API_URL_BASE = "https://pf2020.ddns.net/api";

// Login

const API_URL_LOGIN = `${API_URL_BASE}/login`;
const API_URL_RESTORE_PASSWORD = `${API_URL_BASE}/restore-password`;
const API_URL_CHANGE_PASSWORD = `${API_URL_BASE}/change-password`;

// Usuario
const API_URL_USERS = `${API_URL_BASE}/usuario`;
const API_URL_FIND_USER = `${API_URL_USERS}/like`;

// Libreta
const API_URL_NOTEBOOKS = `${API_URL_BASE}/libreta`;
const API_URL_SHARED_NOTEBOOKS = `${API_URL_NOTEBOOKS}/shared`;

// Notas
const API_URL_NOTES = `${API_URL_BASE}/nota`;
const API_URL_SHARED_NOTES = `${API_URL_NOTES}/shared`;

// Archivos
const API_URL_NOTE_FILES = (idNota) => `${API_URL_NOTES}/${idNota}/archivo`;

module.exports = {
  API_URL_LOGIN,
  API_URL_RESTORE_PASSWORD,
  API_URL_CHANGE_PASSWORD,

  API_URL_USERS,
  API_URL_FIND_USER,

  API_URL_NOTEBOOKS,
  API_URL_SHARED_NOTEBOOKS,

  API_URL_NOTES,
  API_URL_SHARED_NOTES,

  API_URL_NOTE_FILES,
}