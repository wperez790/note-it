const displayNodeResponse = (loadingResponseStatusText, payload, node, msg) => {
  node.status({ fill: 'blue', shape: 'ring', text: loadingResponseStatusText });

  setTimeout(() => {
    node.status({ fill: 'green', shape: 'dot', text: "success" });
  }, 1000);

  setTimeout(() => {
    msg.payload = payload;
    node.send(msg);
  }, 1500);

  setTimeout(() => {
    node.status({ fill: 'green', shape: 'dot', text: "connected" });
  }, 2000);
}

const displayNodeError = (error, node, msg) => {
  node.status({ fill: 'red', shape: 'ring', text: "error" });

  setTimeout(() => {
    msg.payload = {
      response: "Error",
      status: error.response.status,
      message: error.response.data || error.response.statusText
    }
    node.send(msg);
  }, 1000);

  setTimeout(() => {
    node.status({ fill: 'green', shape: 'dot', text: "connected" });
  }, 1500);
}

module.exports = {
  displayNodeResponse,
  displayNodeError
}