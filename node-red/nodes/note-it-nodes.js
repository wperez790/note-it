module.exports = function(RED) {
  'use strict'
  const axios = require('axios');
  const filesize = require('filesize');
  const {
    API_URL_LOGIN,
    API_URL_RESTORE_PASSWORD,
    API_URL_CHANGE_PASSWORD,
  
    API_URL_USERS,
    API_URL_FIND_USER,
  
    API_URL_NOTEBOOKS,
    API_URL_SHARED_NOTEBOOKS,
  
    API_URL_NOTES,
    API_URL_SHARED_NOTES,

    API_URL_NOTE_FILES
  } = require('./utils/constants');

  const { displayNodeResponse, displayNodeError } = require('./utils/node-utils')

  // Login Nodes

	function NoteItLogin(n) {
    RED.nodes.createNode(this, n);

    this.email = n.email;
    this.password = n.password;
    this.verifCode = n.verifCode;

    var node = this;

    const flowContext = node.context().flow;

    node.status({ fill: 'green', shape: 'dot', text: "connected" });

    const login = function(params, msg) {
      if (params.email.length === 0) params.email = null;
      if (params.password.length === 0) params.password = null;
      if (params.verifCode.length === 0) params.verifCode = null;

      let data = {
        email: params.email,
        password: params.password,
      }

      if (params.verifCode) data.verificationCode = params.verifCode;

      if (msg) {
        (async () => {
          try {
            const response = await axios.post(`${API_URL_LOGIN}`, data);

            flowContext.set('token', response.data.token);

             const payload = {
              response: "Success",
              status: response.status,
              token: response.data.token
            }

            displayNodeResponse(
              "logging in..",
              payload,
              node,
              msg
            );  
          } catch (error) {
            displayNodeError(error, node, msg);
          }
        })();
      }
    }
    
    let params = {};
    
    node.on('input', function(msg) {
      params.email = node.email ? node.email : (msg.payload && msg.payload.email ? msg.payload.email : "");
      params.password = node.password ? node.password : (msg.payload && msg.payload.password ? msg.payload.password : "");
      params.verifCode = node.verifCode ? node.verifCode : (msg.payload && msg.payload.verifCode ? msg.payload.verifCode : "");
      
      login(params, msg);
    });
    
		node.on('close', function(done) {
      node.status({});
      params.email = "";
      params.password = "";
      params.verifCode = "";
      done();
    });
  }

  RED.nodes.registerType('note-it login', NoteItLogin);
  
  function NoteItRestorePassword(n) {
    RED.nodes.createNode(this, n);

    this.email = n.email;

    var node = this;

    node.status({ fill: 'green', shape: 'dot', text: "connected" });

    const restorePassword = function(params, msg) {
      if (params.email.length === 0) params.email = null;

      const data = {
        email: params.email
      }

      if (msg) {
        (async () => {
          try {
            const response = await axios.post(`${API_URL_RESTORE_PASSWORD}`, data);

            const payload = {
              response: "Success",
              status: response.status,
              message: "In order to change the password, you must enter the verification code sent to your email."
            }

            displayNodeResponse(
              "sending email..",
              payload,
              node,
              msg
            );
          } catch (error) {
            displayNodeError(error, node, msg);
          }
        })();
      }
    }

    let params = {};

    node.on('input', function(msg) {
      params.email = node.email ? node.email : (msg.payload && msg.payload.email ? msg.payload.email : "");

      restorePassword(params, msg);
    });

    node.on('close', function(done) {
      node.status({});
      params.email = "";
      done();
    });
  }

  RED.nodes.registerType('note-it restore password', NoteItRestorePassword);

  function NoteItChangePassword(n) {
    RED.nodes.createNode(this, n);

    this.newPassword = n.newPassword;
    this.verifCode = n.verifCode;

    var node = this;

    const flowContext = node.context().flow

    node.status({ fill: 'green', shape: 'dot', text: "connected" });

    const changePassword = function(params, msg) {
      if (params.newPassword.length === 0) params.newPassword = null;
      if (params.verifCode.length === 0) params.verifCode = null;

      const data = {
        password: params.newPassword,
        verificationCode: params.verifCode
      }

      if (msg) {
        (async () => {
          try {
            const response = await axios.post(`${API_URL_CHANGE_PASSWORD}`, data);

            // Clear 'token' context value
            flowContext.set("token", undefined);

            const payload = {
              response: "Success",
              status: response.status,
              message: "Password changed successfully."
            }

            displayNodeResponse(
              "restoring password..",
              payload,
              node,
              msg
            );
          } catch (error) {
            displayNodeError(error, node, msg);
          }
        })();
      }
    }

    let params = {};

    node.on('input', function(msg) {
      params.newPassword = node.newPassword ? node.newPassword : (msg.payload && msg.payload.newPassword ? msg.payload.newPassword : "");
      params.verifCode = node.verifCode ? node.verifCode : (msg.payload && msg.payload.verifCode ? msg.payload.verifCode : "");

      changePassword(params, msg);
    });

    node.on('close', function(done) {
      node.status({});
      params.newPassword = "";
      params.verifCode = "";
      done();
    });
  }

  RED.nodes.registerType('note-it change password', NoteItChangePassword);

  // User nodes

  function NoteItCreateUser(n) {
    RED.nodes.createNode(this, n);

    this.firstName = n.firstName;
    this.lastName = n.lastName;
    this.email = n.email;
    this.password = n.password;

    var node = this;

    node.status({ fill: 'green', shape: 'dot', text: "connected" });

    const createUser = function(params, msg) {
      if (params.firstName.length === 0) params.firstName = null;
      if (params.lastName.length === 0) params.lastName = null;
      if (params.email.length === 0) params.email = null;
      if (params.password.length === 0) params.password = null;

      const data = {
        name: params.firstName,
        surname: params.lastName,
        email: params.email,
        password: params.password
      }

      if (msg) {
        (async () => {
          try {
            const response = await axios.post(`${API_URL_USERS}`, data);

            const payload = {
              response: "Success",
              status: response.status,
              message: "User registered successfully. In order to enter the platform, you must verify your email with the verification code sent to your email."
            }

            displayNodeResponse(
              "registering user..",
              payload,
              node,
              msg
            );
          } catch (error) {
            displayNodeError(error, node, msg);
          }
        })();
      }
    }

    let params = {};

    node.on('input', function(msg) {
      params.firstName = node.firstName ? node.firstName : (msg.payload && msg.payload.firstName ? msg.payload.firstName : "");
      params.lastName = node.lastName ? node.lastName : (msg.payload && msg.payload.lastName ? msg.payload.lastName : "");
      params.email = node.email ? node.email : (msg.payload && msg.payload.email ? msg.payload.email : "");
      params.password = node.password ? node.password : (msg.payload && msg.payload.password ? msg.payload.password : "");

      createUser(params, msg);
    });

		node.on('close', function(done) {
      node.status({});
      params.firstName = "";
      params.lastName = "";
      params.email = "";
      params.password = "";
      done();
    });
  }

  RED.nodes.registerType('note-it create user', NoteItCreateUser);

  function NoteItUpdateUser(n) {
    RED.nodes.createNode(this, n);

    this.firstName = n.firstName;
    this.lastName = n.lastName;
    this.email = n.email;
    this.password = n.password;

    var node = this;

    const flowContext = node.context().flow

    node.status({ fill: 'green', shape: 'dot', text: "connected" });

    const updateUser = function(params, msg) {
      if (params.firstName.length === 0) params.firstName = null;
      if (params.lastName.length === 0) params.lastName = null;
      if (params.email.length === 0) params.email = null;
      if (params.password.length === 0) params.password = null;

      const config = {
        headers: { "Authorization": `Bearer ${flowContext.get("token")}` }
      }

      let data = {}

      if (params.firstName) data["name"] = params.firstName;
      if (params.lastName) data["surname"] = params.lastName;
      if (params.email) data["email"] = params.email;
      if (params.password) data["password"] = params.password;

      if (msg) {
        (async () => {
          try {
            const response = await axios({
              method: "PUT",
              url: `${API_URL_USERS}`,
              headers: config.headers,
              data
            });

            // Clear 'token' context value
            flowContext.set("token", undefined);

            const payload = {
              response: "Success",
              status: response.status,
              token: response.data.token
            }

            displayNodeResponse(
              "updating user..",
              payload,
              node,
              msg
            );
          } catch (error) {
            displayNodeError(error, node, msg);
          }
        })();
      }
    }

    let params = {};

    node.on('input', function(msg) {
      params.firstName = node.firstName ? node.firstName : (msg.payload && msg.payload.firstName ? msg.payload.firstName : "");
      params.lastName = node.lastName ? node.lastName : (msg.payload && msg.payload.lastName ? msg.payload.lastName : "");
      params.email = node.email ? node.email : (msg.payload && msg.payload.email ? msg.payload.email : "");
      params.password = node.password ? node.password : (msg.payload && msg.payload.password ? msg.payload.password : "");

      updateUser(params, msg);
    });

		node.on('close', function(done) {
      node.status({});
      params.firstName = "";
      params.lastName = "";
      params.email = "";
      params.password = "";
      done();
    });
  }

  RED.nodes.registerType('note-it update user', NoteItUpdateUser);

  function NoteItGetCurrentUser(n) {
    RED.nodes.createNode(this, n);

    var node = this;

    const flowContext = node.context().flow;

    node.status({ fill: 'green', shape: 'dot', text: "connected" });

    const getCurrentUser = function(msg) {
      const config = {
        headers: { "Authorization": `Bearer ${flowContext.get("token")}` }
      }

      if (msg) {
        (async () => {
          try {
            const response = await axios.get(`${API_URL_USERS}`, config);

            const payload = {
              response: "Success",
              status: response.status,
              user: response.data
            }

            displayNodeResponse(
              "getting current user..",
              payload,
              node,
              msg
            );
          } catch (error) {
            displayNodeError(error, node, msg);
          }
        })();
      }
    }

    node.on('input', function(msg) {
      getCurrentUser(msg);
    });

		node.on('close', function(done) {
      node.status({});
      done();
    });
  }

  RED.nodes.registerType('note-it get current user', NoteItGetCurrentUser);

  function NoteItFindUser(n) {
    RED.nodes.createNode(this, n);

    this.search = n.search;

    var node = this;

    const flowContext = node.context().flow;

    node.status({ fill: 'green', shape: 'dot', text: "connected" });

    const findUser = function(params, msg) {
      const config = { headers: { "Authorization": `Bearer ${flowContext.get("token")}` } }

      if (msg) {
        (async () => {
          try {
            const response = await axios.get(`${API_URL_FIND_USER}/${encodeURI(params.search)}`, config);

            const payload = {
              response: "Success",
              status: response.status,
              data: {
                size: response.data.length,
                users: response.data
              }
            }

            displayNodeResponse(
              "searching users..",
              payload,
              node,
              msg
            );
          } catch (error) {
            if (/404/.test(error.response.status)) {
              error.response.data = "No users found";
              error.response.statusText = "No users found";
            }

            displayNodeError(error, node, msg);
          }
        })();
      }
    }

    let params = {};

    node.on('input', function(msg) {
      params.search =
        node.search ? node.search :
        (msg.payload && msg.payload.search) ? msg.payload.search :
        "";

      findUser(params, msg);
    });

		node.on('close', function(done) {
      node.status({});
      params.search = "";
      done();
    });
  }

  RED.nodes.registerType('note-it find user', NoteItFindUser);


  // Notebook nodes

  function NoteItCreateNotebook(n) {
    RED.nodes.createNode(this, n);

    this.title = n.title;
    this.description = n.description;

    var node = this;

    const flowContext = node.context().flow;

    node.status({ fill: 'green', shape: 'dot', text: "connected" });

    const createNotebook = function(params, msg) {
      if (params.title.length === 0) params.title = null;
      if (params.description.length === 0) params.description = null;

      const config = {
        headers: { "Authorization": `Bearer ${flowContext.get("token")}` }
      }

      const data = {
        titulo: params.title,
        descripcion: params.description
      }

      if (msg) {
        (async () => {
          try {
            const response = await axios({
              method: "POST",
              url: `${API_URL_NOTEBOOKS}`,
              headers: config.headers,
              data
            });

            const payload = {
              response: "Success",
              status: response.status,
              message: "Notebook created successfully."
            }

            displayNodeResponse(
              "creating notebook..",
              payload,
              node,
              msg
            );
          } catch (error) {
            displayNodeError(error, node, msg);
          }
        })();
      }
    }

    let params = {};

    node.on('input', function(msg) {
      params.title = node.title ? node.title : (msg.payload && msg.payload.title ? msg.payload.title : "");
      params.description = node.description ? node.description : (msg.payload && msg.payload.description ? msg.payload.description : "");

      createNotebook(params, msg);
    });

		node.on('close', function(done) {
      node.status({});
      params.title = "";
      params.description = "";
      done();
    });
  }

  RED.nodes.registerType('note-it create notebook', NoteItCreateNotebook);

  function NoteItUpdateNotebook(n) {
    RED.nodes.createNode(this, n);

    this.title = n.title;
    this.description = n.description;
    this.participants = n.participants;

    var node = this;

    const flowContext = node.context().flow;

    node.status({ fill: 'green', shape: 'dot', text: "connected" });

    const updateNotebook = function(params, msg) {
      if (params.description.length === 0) params.description = null;
      if (params.participants.length === 0) params.participants = null;

      const config = {
        headers: { "Authorization": `Bearer ${flowContext.get("token")}` }
      }

      let data = {};

      if (params.description) data.descripcion = params.description;

      if (msg) {
        (async () => {
          try {
            if (params.participants) {
              const participantsUpdated = await Promise.all(
                Array.from(params.participants).map(async (participant) => {
                  const response = await axios.get(`${API_URL_FIND_USER}/${encodeURI(participant.email)}`, config);
                  participant["id"] = response.data[0]["_id"];
                  participant["permisos"] = participant.permissions;
                  delete participant.email;
                  delete participant.permissions;
                  return participant;
                })
              );
              data.participantes = participantsUpdated;
            }

            if (params.title.length > 0) {
              const notebooks = await axios.get(`${API_URL_NOTEBOOKS}`, config);
              
              if (notebooks.data.length > 0) {
                const notebook = notebooks.data.find(libreta => libreta.titulo === params.title);
                if (notebook) {
                  data["_id"] = notebook["_id"];
                }
              }

              const sharedNotebooks = await axios.get(`${API_URL_SHARED_NOTEBOOKS}`, config);

              if (sharedNotebooks.data.length > 0) {
                const notebook = sharedNotebooks.data.find(libreta => libreta.titulo === params.title);
                if (notebook) {
                  data["_id"] = notebook["_id"];
                }
              }
            }

            const response = await axios({
              method: "PUT",
              url: `${API_URL_NOTEBOOKS}`,
              headers: config.headers,
              data
            });

            const payload = {
              response: "Success",
              status: response.status,
              message: "Notebook updated successfully."
            }

            displayNodeResponse(
              "updating notebook..",
              payload,
              node,
              msg
            );
          } catch (error) {
            if (error.response.data.includes('_id field') || error.response.statusText.includes('_id field')) {
              error.response.data = "Notebook not found";
              error.response.statusText = "Notebook not found";
            }
            displayNodeError(error, node, msg);
          }
        })();
      }
    }

    let params = {};

    node.on('input', function(msg) {
      params.title = node.title ? node.title : (msg.payload && msg.payload.title ? msg.payload.title : "");
      params.description = node.description ? node.description : (msg.payload && msg.payload.description ? msg.payload.description : "");
      params.participants = node.participants ? node.participants : (msg.payload && msg.payload.participants ? msg.payload.participants : "");

      updateNotebook(params, msg);
    });

		node.on('close', function(done) {
      node.status({});
      params.title = "";
      params.description = "";
      params.participants = "";
      done();
    });
  }

  RED.nodes.registerType('note-it update notebook', NoteItUpdateNotebook);

  function NoteItListNotebooks(n) {
    RED.nodes.createNode(this, n);

    var node = this;

    const flowContext = node.context().flow;

    node.status({ fill: 'green', shape: 'dot', text: "connected" });

    const listNotebooks = function(msg) {
      const config = {
        headers: { "Authorization": `Bearer ${flowContext.get("token")}` }
      }

      if (msg) {
        (async () => {
          try {
            const response = await axios.get(`${API_URL_NOTEBOOKS}`, config);

            if (response.data.length > 0) {
              const currentUser = await axios.get(`${API_URL_USERS}`, config);

              const notebooksUpdated = await Promise.all(response.data.map(notebook => {
                notebook.creador = currentUser.data;

                if (notebook.participantes) {
                  notebook.participantes.map(async (participant) => {
                    const response = await axios.get(`${API_URL_USERS}/${participant.id}`, config);
                    participant.name = response.data.name;
                    participant.surname = response.data.surname;
                    participant.email = response.data.email;
                    delete participant.id;
                    return participant;
                  })
                }

                delete notebook["id_creador"];

                return notebook;
              }))
              response.data = notebooksUpdated;
            }

            const payload = {
              response: "Success",
              status: response.status,
              data: {
                size: response.data.length,
                notebooks: response.data
              }
            }

            displayNodeResponse(
              "getting notebooks..",
              payload,
              node,
              msg
            );
          } catch (error) {
            displayNodeError(error, node, msg);
          }
        })();
      }
    }

    node.on('input', function(msg) {
      listNotebooks(msg);
    });

		node.on('close', function(done) {
      node.status({});
      done();
    });
  }

  RED.nodes.registerType('note-it list notebooks', NoteItListNotebooks);

  function NoteItListSharedNotebooks(n) {
    RED.nodes.createNode(this, n);

    var node = this;

    const flowContext = node.context().flow;

    node.status({ fill: 'green', shape: 'dot', text: "connected" });

    const listSharedNotebooks = function(msg) {
      const config = {
        headers: { "Authorization": `Bearer ${flowContext.get("token")}` }
      }

      if (msg) {
        (async () => {
          try {
            const response = await axios.get(`${API_URL_SHARED_NOTEBOOKS}`, config);

            if (response.data.length > 0) {
              const notebooksUpdated = await Promise.all(
                response.data.map(async (notebook) => {
                  const creator = await axios.get(`${API_URL_USERS}/${notebook.id_creador}`, config);
                  notebook.creador = creator.data;

                  if (notebook.participantes) {
                    notebook.participantes.map(async (participant) => {
                      const response = await axios.get(`${API_URL_USERS}/${participant.id}`, config);
                      participant.name = response.data.name;
                      participant.surname = response.data.surname;
                      participant.email = response.data.email;
                      delete participant.id;
                      return participant;
                    })
                  }

                  delete notebook["id_creador"];

                  return notebook;
                })
              )
              response.data = notebooksUpdated;
            }

            const payload = {
              response: "Success",
              status: response.status,
              data: {
                size: response.data.length,
                sharedNotebooks: response.data
              }
            }

            displayNodeResponse(
              "getting shared notebooks..",
              payload,
              node,
              msg
            );
          } catch (error) {
            displayNodeError(error, node, msg);
          }
        })();
      }
    }

    node.on('input', function(msg) {
      listSharedNotebooks(msg);
    });

		node.on('close', function(done) {
      node.status({});
      done();
    });
  }

  RED.nodes.registerType('note-it list shared notebooks', NoteItListSharedNotebooks);


  // Note nodes

  function NoteItCreateNote(n) {
    RED.nodes.createNode(this, n);

    this.title = n.title;
    this.notebook = n.notebook;

    var node = this;

    const flowContext = node.context().flow;

    node.status({ fill: 'green', shape: 'dot', text: "connected" });

    const createNote = function(params, msg) {
      if (params.title.length === 0) params.title = null;

      const config = {
        headers: { "Authorization": `Bearer ${flowContext.get("token")}` }
      }

      let data = {
        titulo: params.title
      }

      if (msg) {
        (async () => {
          try {
            if (params.notebook.length > 0) {
              const notebooks = await axios.get(`${API_URL_NOTEBOOKS}`, config);
              
              if (notebooks.data.length > 0) {
                const notebook = notebooks.data.find(libreta => libreta.titulo === params.notebook);
                if (notebook) {
                  data["id_libreta"] = notebook["_id"];
                }
              }

              const sharedNotebooks = await axios.get(`${API_URL_SHARED_NOTEBOOKS}`, config);

              if (sharedNotebooks.data.length > 0) {
                const notebook = sharedNotebooks.data.find(libreta => libreta.titulo === params.notebook);
                if (notebook) {
                  data["id_libreta"] = notebook["_id"];
                }
              }
            }

            const response = await axios({
              method: "POST",
              url: `${API_URL_NOTES}`,
              headers: config.headers,
              data
            });

            const payload = {
              response: "Success",
              status: response.status,
              message: "Note created successfully."
            }

            displayNodeResponse(
              "creating note..",
              payload,
              node,
              msg
            );
          } catch (error) {
            if (/Invalid notebook id/.test(error.response.data || error.response.statusText)) {
              error.response.data = "Notebook not found";
              error.response.statusText = "Notebook not found";
            }
            displayNodeError(error, node, msg);
          }
        })();
      }
    }

    let params = {};

    node.on('input', function(msg) {
      params.title = node.title ? node.title : (msg.payload && msg.payload.title ? msg.payload.title : "");
      params.notebook = node.notebook ? node.notebook : (msg.payload && msg.payload.notebook ? msg.payload.notebook : "");

      createNote(params, msg);
    });

		node.on('close', function(done) {
      node.status({});
      params.title = "";
      params.notebook = "";
      done();
    });
  }

  RED.nodes.registerType('note-it create note', NoteItCreateNote);

  function NoteItUpdateNote(n) {
    RED.nodes.createNode(this, n);

    this.title = n.title;
    this.notebook = n.notebook;
    this.detail = n.detail;
    this.participants = n.participants;
    this.checklist = n.checklist;

    var node = this;

    const flowContext = node.context().flow;

    node.status({ fill: 'green', shape: 'dot', text: "connected" });

    const updateNote = function(params, msg) {
      if (params.detail.length === 0) params.detail = null;
      if (params.participants.length === 0) params.participants = null;
      if (params.checklist.length === 0) params.checklist = null;

      const config = {
        headers: { "Authorization": `Bearer ${flowContext.get("token")}` }
      }

      let data = {};

      if (params.detail) data.detalle = params.detail;
      if (params.checklist) data.checklist = params.checklist;

      if (msg) {
        (async () => {
          try {
            if (params.participants) {
              const participantsUpdated = await Promise.all(
                params.participants.map(async (participant) => {
                  const response = await axios.get(`${API_URL_FIND_USER}/${encodeURI(participant.email)}`, config);
                  participant["id"] = response.data[0]["_id"];
                  participant["permisos"] = participant.permissions;
                  delete participant.email;
                  delete participant.permissions;
                  return participant;
                })
              );
              data.participantes = participantsUpdated;
            }

            if (params.title.length > 0) {
              if (params.notebook.length > 0) {
                let notebookId = "";
                const notebooks = await axios.get(`${API_URL_NOTEBOOKS}`, config);
  
                if (notebooks.data.length > 0) {
                  const notebook = notebooks.data.find(libreta => libreta.titulo === params.notebook);
                  if (notebook) {
                    notebookId = notebook["_id"];
                  }
                }

                const notesFromNotebook = await axios.get(`${API_URL_NOTES}?id_libreta=${encodeURI(notebookId) || params.notebook}`, config);

                if (notesFromNotebook.data.length > 0) {
                  const note = notesFromNotebook.data.find(nota => nota.titulo === params.title);
                  if (note) {
                    data["_id"] = note["_id"];
                  }
                }
              }
              if (!params.notebook) {
                const notes = await axios.get(`${API_URL_NOTES}`, config);
  
                if (notes.data.length > 0) {
                  const note = notes.data.find(nota => nota.titulo === params.title);
                  if (note) {
                    data["_id"] = note["_id"];
                  }
                }
              }
            }

            const response = await axios({
              method: "PUT",
              url: `${API_URL_NOTES}`,
              headers: config.headers,
              data
            });

            const payload = {
              response: "Success",
              status: response.status,
              message: "Note updated successfully."
            }

            displayNodeResponse(
              "updating note..",
              payload,
              node,
              msg
            );
          } catch (error) {
            // if (error.response.data.includes('_id field') || error.response.statusText.includes('_id field')) {
            //   error.response.data = "Notebook not found";
            //   error.response.statusText = "Notebook not found";
            // }
            displayNodeError(error, node, msg);
          }
        })();
      }
    }

    let params = {};

    node.on('input', function(msg) {
      params.title = node.title ? node.title : (msg.payload && msg.payload.title ? msg.payload.title : "");
      params.notebook = node.notebook ? node.notebook : (msg.payload && msg.payload.notebook ? msg.payload.notebook : "");
      params.detail = node.detail ? node.detail : (msg.payload && msg.payload.detail ? msg.payload.detail : "");
      params.participants = node.participants ? node.participants : (msg.payload && msg.payload.participants ? msg.payload.participants : "");
      params.checklist = node.checklist ? node.checklist : (msg.payload && msg.payload.checklist ? msg.payload.checklist : "");

      updateNote(params, msg);
    });

		node.on('close', function(done) {
      node.status({});
      params.title = "";
      params.detail = "";
      params.participants = [];
      params.checklist = [];
      done();
    });
  }

  RED.nodes.registerType('note-it update note', NoteItUpdateNote);

  function NoteItListNotesWithoutNotebook(n) {
    RED.nodes.createNode(this, n);

    var node = this;

    const flowContext = node.context().flow;

    node.status({ fill: 'green', shape: 'dot', text: "connected" });

    const listNotesWithoutNotebook = function(msg) {
      const config = {
        headers: { "Authorization": `Bearer ${flowContext.get("token")}` }
      }

      if (msg) {
        (async () => {
          try {
            const response = await axios({
              method: "GET",
              url: `${API_URL_NOTES}`,
              headers: config.headers
            });

            if (response.data.length > 0) {
              const currentUser = await axios.get(`${API_URL_USERS}`, config);

              const notesUpdated = await Promise.all(response.data.map(note => {
                note.creador = currentUser.data;

                if (note.participantes) {
                  note.participantes.map(async (participant) => {
                    const response = await axios.get(`${API_URL_USERS}/${participant.id}`, config);
                    participant.name = response.data.name;
                    participant.surname = response.data.surname;
                    participant.email = response.data.email;
                    delete participant.id;
                    return participant;
                  })
                }

                delete note["id_creador"];

                return note;
              }))
              response.data = notesUpdated;
            }

            const payload = {
              response: "Success",
              status: response.status,
              data: {
                size: response.data.length,
                notes: response.data
              }
            }

            displayNodeResponse(
              "getting notes without notebook..",
              payload,
              node,
              msg
            );
          } catch (error) {
            displayNodeError(error, node, msg);
          }
        })();
      }
    }

    node.on('input', function(msg) {
      listNotesWithoutNotebook(msg);
    });

		node.on('close', function(done) {
      node.status({});
      done();
    });
  }

  RED.nodes.registerType('note-it list notes without notebook', NoteItListNotesWithoutNotebook);

  function NoteItListSharedNotesWithoutNotebook(n) {
    RED.nodes.createNode(this, n);

    var node = this;

    const flowContext = node.context().flow;

    node.status({ fill: 'green', shape: 'dot', text: "connected" });

    const listSharedNotesWithoutNotebook = function(msg) {
      const config = {
        headers: { "Authorization": `Bearer ${flowContext.get("token")}` }
      }

      if (msg) {
        (async () => {
          try {
            const response = await axios({
              method: "GET",
              url: `${API_URL_SHARED_NOTES}`,
              headers: config.headers
            });

            if (response.data.length > 0) {
              const notesUpdated = await Promise.all(
                response.data.map(async (note) => {
                  const creator = await axios.get(`${API_URL_USERS}/${note.id_creador}`, config);
                  note.creador = creator.data;

                  if (note.participantes) {
                    note.participantes.map(async (participant) => {
                      const response = await axios.get(`${API_URL_USERS}/${participant.id}`, config);
                      participant.name = response.data.name;
                      participant.surname = response.data.surname;
                      participant.email = response.data.email;
                      delete participant.id;
                      return participant;
                    })
                  }

                  delete note["id_creador"];

                  return note;
                })
              )
              response.data = notesUpdated;
            }

            const payload = {
              response: "Success",
              status: response.status,
              data: {
                size: response.data.length,
                sharedNotes: response.data
              }
            }

            displayNodeResponse(
              "getting shared notes without notebook..",
              payload,
              node,
              msg
            );
          } catch (error) {
            displayNodeError(error, node, msg);
          }
        })();
      }
    }

    node.on('input', function(msg) {
      listSharedNotesWithoutNotebook(msg);
    });

		node.on('close', function(done) {
      node.status({});
      done();
    });
  }

  RED.nodes.registerType('note-it list shared notes without notebook', NoteItListSharedNotesWithoutNotebook);

  function NoteItListNotebookNotes(n) {
    RED.nodes.createNode(this, n);

    this.title = n.title;

    var node = this;

    const flowContext = node.context().flow;

    node.status({ fill: 'green', shape: 'dot', text: "connected" });

    const listNotebookNotes = function(params, msg) {
      const config = { headers: { "Authorization": `Bearer ${flowContext.get("token")}` } }

      let notebookId = '';

      if (msg) {
        (async () => {
          try {
            if (params.title.length > 0) {
              const notebooks = await axios.get(`${API_URL_NOTEBOOKS}`, config);

              if (notebooks.data.length > 0) {
                const notebook = notebooks.data.find(libreta => libreta.titulo === params.title);
                if (notebook) {
                  notebookId = notebook["_id"];
                }
              }

              const sharedNotebooks = await axios.get(`${API_URL_SHARED_NOTEBOOKS}`, config);

              if (sharedNotebooks.data.length > 0) {
                const notebook = sharedNotebooks.data.find(libreta => libreta.titulo === params.title);
                if (notebook) {
                  notebookId = notebook["_id"];
                }
              }
            }

            const response = await axios.get(`${API_URL_NOTES}?id_libreta=${encodeURI(notebookId) || params.title}`, config);

            if (response.data.length > 0) {
              const currentUser = await axios.get(`${API_URL_USERS}`, config);

              const notesUpdated = await Promise.all(response.data.map(note => {
                note.creador = currentUser.data;

                if (note.participantes) {
                  note.participantes.map(async (participant) => {
                    const response = await axios.get(`${API_URL_USERS}/${participant.id}`, config);
                    participant.name = response.data.name;
                    participant.surname = response.data.surname;
                    participant.email = response.data.email;
                    delete participant.id;
                    return participant;
                  })
                }

                delete note["id_creador"];

                return note;
              }))
              response.data = notesUpdated;
            }

            const payload = {
              response: "Success",
              status: response.status,
              data: {
                size: response.data.length,
                notes: response.data
              }
            }

            displayNodeResponse(
              "getting notebook notes..",
              payload,
              node,
              msg
            );
          } catch (error) {
            if (/Bad notebook id/.test(error.response.data || error.response.statusText)) {
              error.response.data = "Notebook not found";
              error.response.statusText = "Notebook not found";
            }

            if (/404/.test(error.response.status)) {
              error.response.data = "Zero Notes found";
              error.response.statusText = "Zero notes found";
            }

            displayNodeError(error, node, msg);
          }
        })();
      }
    }

    let params = {};

    node.on('input', function(msg) {
      params.title =
        node.title ? node.title :
        (msg.payload && msg.payload.title) ? msg.payload.title :
        "";

      listNotebookNotes(params, msg);
    });

		node.on('close', function(done) {
      node.status({});
      params.title = "";
      done();
    });
  }

  RED.nodes.registerType('note-it list notebook notes', NoteItListNotebookNotes);

  // File Nodes

  function NoteItNoteListFiles(n) {
    RED.nodes.createNode(this, n);

    this.title = n.title;
    this.notebook = n.notebook;

    var node = this;

    const flowContext = node.context().flow;

    let noteId = '';

    node.status({ fill: 'green', shape: 'dot', text: "connected" });

    const listNoteFiles = function(params, msg) {
      const config = { headers: { "Authorization": `Bearer ${flowContext.get("token")}` } }

      if (msg) {
        (async () => {
          try {
            if (params.title.length > 0) {
              if (params.notebook.length > 0) {
                let notebookId = '';
                const notebooks = await axios.get(`${API_URL_NOTEBOOKS}`, config);
                
                if (notebooks.data.length > 0) {
                  const notebook = notebooks.data.find(libreta => libreta.titulo === params.notebook);
                  if (notebook) {
                    notebookId = notebook["_id"];
                  }
                }

                const sharedNotebooks = await axios.get(`${API_URL_SHARED_NOTEBOOKS}`, config);

                if (sharedNotebooks.data.length > 0) {
                  const notebook = sharedNotebooks.data.find(libreta => libreta.titulo === params.notebook);
                  if (notebook) {
                    notebookId = notebook["_id"];
                  }
                }

                if (notebookId) {
                  const notes = await axios.get(`${API_URL_NOTES}?id_libreta=${notebookId || params.notebook}`, config);

                  if (notes.data.length > 0) {
                    const note = notes.data.find(nota => nota.titulo === params.title);
                    if (note) {
                      noteId = note["_id"];
                    }
                  }
                }
              } else {
                const notes = await axios.get(`${API_URL_NOTES}`, config);

                if (notes.data.length > 0) {
                  const note = notes.data.find(nota => nota.titulo === params.title);
                  if (note) {
                    noteId = note["_id"];
                  }
                }

                const sharedNotes = await axios.get(`${API_URL_SHARED_NOTES}`, config);

                if (sharedNotes.data.length > 0) {
                  const note = sharedNotes.data.find(nota => nota.titulo === params.title);
                  if (note) {
                    noteId = note["_id"];
                  }
                }
              }
            }

            const response = await axios({
              url: `${API_URL_NOTE_FILES(noteId || null)}`,
              method: 'GET',
              headers: config.headers
            });

            const files = response.data.map(f => (Object.assign(f, {'tamaño': filesize(f['tamaño'],{round:0})})));

            const payload = {
              response: "Success",
              status: response.status,
              data: {
                size: response.data.length,
                files
              }
            }

            displayNodeResponse(
              "getting note files..",
              payload,
              node,
              msg
            );
          } catch (error) {
            if (/Invalid Note Id/.test(error.response.data || error.response.statusText)) {
              error.response.data = "Note not found";
              error.response.statusText = "Note not found";
            }

            if (/404/.test(error.response.status)) {
              error.response.data = "Zero Notes found";
              error.response.statusText = "Zero notes found";
            }

            displayNodeError(error, node, msg);
          }
        })();
      }
    }

    let params = {};

    node.on('input', function(msg) {
      params.title =
        node.title ? node.title :
        (msg.payload && msg.payload.title) ? msg.payload.title :
        "";
      params.notebook =
        node.notebook ? node.notebook :
        (msg.payload && msg.payload.notebook) ? msg.payload.notebook :
        "";

      listNoteFiles(params, msg);
    });

		node.on('close', function(done) {
      node.status({});
      params.title = "";
      params.notebook = "";
      done();
    });
  }

  RED.nodes.registerType('note-it note list files', NoteItNoteListFiles);
}