  
import React,  { useState, useEffect} from 'react';
import {Link, withRouter} from "react-router-dom";
import '../App.css';
import { useHistory } from "react-router-dom";
import axios from 'axios'
import AddIcon from '@material-ui/icons/AddCircleOutlineRounded';
import { withStyles, makeStyles } from '@material-ui/core/styles';
import {Input, InputLabel, Divider, Card, Grid, Box, CardContent, Typography, Button} from '@material-ui/core';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import swal from 'sweetalert';
import Autocomplete from '@material-ui/lab/Autocomplete';
import {urlBackend, notebookEndpoint, noteEndpoint, findUser} from '../utils/constants';
const { ipcRenderer } = window.require('electron');

const useStyles = makeStyles((theme) => ({
  root: {
    '& > *': {
      margin: theme.spacing(1),
    },
  },
  "form__input-control": {
    width: "100%",
    marginBottom: ".6rem"
  },
  "form__input": {
    fontFamily: "Montserrat, sans-serif",
    fontWeight: 400
  },
  "link": {
    color: "#0288D1"
  },
  "label__register-question": {
    fontSize: ".8rem"
  },
  textField: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
    width: '25ch',
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
    justifyContent: 'flex-start'
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
  button: {
    backgroundColor: "gray",
    margin: 10
  },
  "card__notes": {
    margin: 10,
    backgroundColor: "lightgray",
  },
}));

 function NewNote(props){
  const classes = useStyles();
  const history = useHistory();
  const [text, setText] = useState("text")
  const [startDate, setStartDate] = useState(new Date())
  const [endDate, setEndDate] = useState(new Date())
  const [checkArray, setCheckArray] = useState([{"desc":"Nuevo Check", "value": false}])
  const [myNotebooks, setMyNotebooks] = useState([])
  const [notebookSelected, setNotebookSelected] = useState("")
  const [users, setUsers] = useState([])
  const [userSelected, setUserSelected] = useState({})
  const [arrayParticipantes, setArrayParticipantes] = useState([])
  const [participantesToShow, setParticipantesToShow] = useState([])
 

  function handleAddCheck(){
    let newcheck = [{"desc":"Nuevo Check", "value": false}]
    setCheckArray(prevArray => ([...prevArray, ...newcheck]))
  }


  function handleSelectNotebook(e){
    setNotebookSelected(e.target.value)
  }
  function handleSubmit(event){
    event.preventDefault()
    const form = event.target.elements
    let data_= 
    {
      'titulo': form.titulo.value,
      'descripcion': form.descripcion.value,
    }
    if(!data_.titulo || !data_.descripcion){
      swal("Complete algunos datos", "Se debe completar titulo y descripcion", "error")
      return
    }
    if(arrayParticipantes?.length > 0){
      data_.participantes = arrayParticipantes
    }
    if(notebookSelected){
      data_.id_libreta = notebookSelected
    }
    if(checkArray?.length > 0){
      data_.checklist = checkArray
    }
    axios({
        method: 'post',
        url: urlBackend+noteEndpoint,
        headers:{
          'Authorization': `Bearer ${localStorage.getItem("jwt")}`
        },
        data: data_,
    })
    .then(async res => {
      swal("Nota Creada con éxito", "Se agrego a mis notas", "success")
      let array = JSON.parse(localStorage.getItem("recentNotes")) || []
      if(!array?.includes(res.data.id)){
        array.push(res.data.id)
        localStorage.setItem("recentNotes", JSON.stringify(array));
      }
    })
    .catch(function (error) {
        console.log(error);
      });
    
}


function handleSelectUser(user){
  setUserSelected(user)
}
function handleCheck(index){
  let newCheckArray = [...checkArray];
  let newCheck = checkArray[index];
  newCheck.value = !newCheck.value;
  newCheckArray[index] = newCheck
  setCheckArray(newCheckArray)
}
async function handleChangeUser(e){
  let name = e.target.value
  if(name.length >= 3){
    await axios({
      method: 'get',
      url: urlBackend + findUser + name,
      headers:{
        'Authorization': `Bearer ${localStorage.getItem("jwt")}`
      }
    })
    .then(async res => {
        await setUsers(res.data)
    })
    .catch(function (error) {
        console.log(error);
      });
    }
}

function handleCheckDesc(e, index){
  const desc = e.target.value
  let newCheckArray = [...checkArray];
  let newCheck = checkArray[index];
  newCheck.desc = desc;
  newCheckArray[index] = newCheck
  setCheckArray(newCheckArray)

}
function getNotebooks(){
  axios({
    method: 'get',
    url: urlBackend + notebookEndpoint,
    headers:{
      'Authorization': `Bearer ${localStorage.getItem("jwt")}`
    }
})
.then(res => {
    setMyNotebooks(res.data)
})
.catch(function (error) {
    console.log(error);
  });
}
const updateParticipantes= async () => {
  const participante = [{id: userSelected._id, permisos: ["UPDATE", "DELETE"]}]
  const participanteToShow = [{id: userSelected._id, name:`${userSelected.name} ${userSelected.surname}`}] 
  setArrayParticipantes(prevArray => ([...prevArray, ...participante]))
  setParticipantesToShow(prevArray => ([...prevArray, ...participanteToShow]))
}
useEffect(() => {
  getNotebooks();
  if(Object.keys(userSelected).length !== 0 ){
    updateParticipantes();
  }
}, [userSelected]);
  return(
    <div className= "page container__with-bg-shapes">
      <div className="card__container">
        <div className="card__header">
          <h1 className="card__title">Nueva Nota +</h1>
          <div className="card__subtitle"></div>
        <form onSubmit={handleSubmit}>
        <FormControl className={classes.formControl} >
        <InputLabel id="demo-simple-select-helper-label">Libreta</InputLabel>
        <Select
          labelId="demo-simple-select-helper-label"
          id="select"
          //value={}
          onChange={handleSelectNotebook}
        >
          <MenuItem value="Sin asignar">
          <em>Sin asignar</em>
          </MenuItem>
          {myNotebooks.map((notebook, index) => (
          <MenuItem value={notebook._id}>
            {notebook.titulo}
          </MenuItem>
          ))}
        </Select>
        <FormHelperText>Asignación a una libreta</FormHelperText>
      </FormControl>
      <TextField
          id="titulo"
          label="Titulo"
          style={{ margin: 8 }}
          placeholder="Titulo de la Nota"
          helperText=""
          fullWidth
          margin="normal"
          InputLabelProps={{
            shrink: true,
          }}
          
        />
           <TextField
          id="descripcion"
          label="Descripción"
          multiline={true}
          rowsMax={4}
          style={{ margin: 8 }}
          placeholder="Escriba la descripción de la nota aquí"
          helperText=""
          fullWidth
          margin="normal"
          InputLabelProps={{
            shrink: true,
          }}
          variant="filled"
        />
  <Grid md={12} lg={12} sm={12}>
    <Autocomplete
        id="combo-box-demo"
        options={users}
        getOptionLabel={(option) => `${option.name} ${option.surname}`}
        style={{ width: "auto" }}
        getOptionSelected={(option) => handleSelectUser(option)}
        renderInput={(params) => <TextField {...params} label="Participantes" variant="outlined" onChange={handleChangeUser} />}
      />
  </Grid>
  {participantesToShow.length? participantesToShow.map((participante, index) => (
    <div>
    <Card className={classes.card__notes} key={index}>
      <CardContent>
        <Typography  component="h2" >
        {participante.name}
        </Typography>
      </CardContent>
    </Card></div>
  )):null}
  <InputLabel className={classes["form__input"]}>CheckList</InputLabel>
  <Button onClick={() => handleAddCheck()}><AddIcon color="primary" fontSize="large"></AddIcon>Nuevo Check</Button>
  <br />
  
    {checkArray.map((check, index) => (
      <Grid md={12} lg={12} sm={12}>
      <FormControlLabel
        control={
          <Checkbox
            checked={check.value}
            onChange={() => handleCheck(index)}
            name="checks"
            color="primary"
          />
        }
      />
       <Input
        id={index}
        className={classes["form__input"]}
        type='text'
        placeholder= {check.desc}
        onChange={(event) => handleCheckDesc(event, index)}
        />
        </Grid>
    ))}

    <Grid>
      <Button
        fullWidth
        variant="contained"
        color="primary"
        type="submit"
      >
        Agregar
      </Button>
    </Grid>
    </form>       
    </div>
    </div>
    </div>
  );
 }

export default  withRouter (NewNote);