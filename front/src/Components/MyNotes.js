import React,  { useState, useEffect} from 'react';
import {Link, withRouter} from "react-router-dom";
import { useHistory } from "react-router-dom";
import { InputLabel, Divider, Card, Grid, Box, CardContent, Typography, CardHeader, IconButton} from '@material-ui/core';
import { withStyles, makeStyles } from '@material-ui/core/styles';
import MenuItem from '@material-ui/core/MenuItem';
import Select from '@material-ui/core/Select';
import axios from 'axios'
import swal from 'sweetalert'
import ClearIcon from '@material-ui/icons/Clear';
import {urlBackend, notebookEndpoint, noteEndpoint, allNotes, maxString} from '../utils/constants';
import '../App.css';


const useStyles = makeStyles((theme) => ({
    "input__label_title": {
      fontFamily: "Montserrat, sans-serif",
      fontWeight: 400,
      fontSize:100,
      marginTop: 10
    },
    "input__label_subtitle": {
      fontFamily: "Montserrat, sans-serif",
      fontWeight: 400,
      fontSize:30,
      marginTop: 30
    },
    "divider__withmargin": {
      margin: 10
    },
    "card__notes": {
      cursor: "pointer",
      margin: 10,
      transition: "0.3s",
      boxShadow: "0 8px 40px rgba(0,0,0,0.3)",
      "&:hover": {
        boxShadow: "0 16px 70px rgba(0,0,0,0.3)"
      }
    },
    "select":{
        width: "auto",

    },
    withMargin:{
        margin:20
    },
    card__header:{
      height:10
    }
  }));


function MyNotes(props){
    const classes = useStyles();
    const [notes, setNotes] = useState([]);
    const history = useHistory();
    const [notebookSelected, setNotebookSelected] = useState("")
    const [myNotebooks, setMyNotebooks] = useState([])
    const [notesByNotebook, setNotesByNotebook] = useState([])

    async function handleSelectNotebook(e){
      await setNotebookSelected(e.target.value)
      if(e.target.value === "Sin Libreta"){
        getNotes();
      }
      else{
          getNotesByNotebook(e.target.value);
      }
    }

    const getNotes = () => {
    axios({
        method: 'get',
        url: `${urlBackend}${allNotes}`,
        headers:{
        'Authorization': `Bearer ${localStorage.getItem("jwt")}`
        }
    })
    .then(res => {
        console.log(res.data)
        setNotes(res.data)
    })
    .catch(function (error) {
    });
    }
  const handClickOnCard = async note => {
    await props.setNote(note)
    history.push("/notes")
  }
  function getNotebooks(){
    axios({
      method: 'get',
      url: urlBackend + notebookEndpoint,
      headers:{
        'Authorization': `Bearer ${localStorage.getItem("jwt")}`
      }
    })
    .then(res => {
        setMyNotebooks(res.data)
    })
    .catch(function (error) {
        console.log(error);
        });
  }
  const getNotesByNotebook = (id) => {
    axios({
        method: 'get',
        url: urlBackend + noteEndpoint,
        params:{'id_libreta': id},
        headers:{
          'Authorization': `Bearer ${localStorage.getItem("jwt")}`
        }
      })
      .then(res => {
          setNotes(res.data)
      })
      .catch(function (error) {
          console.log(error);
          });
  }
  const handleDeleteNote = (item) =>{
    
    swal({
      title: "¿Esta seguro que desea eliminar la nota?",
      text: "Una vez eliminada no podra recuperarse",
      icon: "warning",
      buttons: true,
      dangerMode: true,
    })
    
    .then((willDelete) => {
      if (willDelete) {
        axios({
          method: 'delete',
          url: urlBackend + noteEndpoint + "/" + item._id ,
          headers:{
            'Authorization': `Bearer ${localStorage.getItem("jwt")}`
          }
        })
          .then(res => {
              swal("La Nota se ha eliminado!", {
                  icon: "success",
              });
              refreshPage()
          })
          .catch(function(error){
                  swal(error);
              })
      } else {
        swal("La Nota no se modifico!");
      }
    });
  }
  useEffect(() => {
    getNotebooks();
    getNotes();
    
  }, []);

    return(
        <div className= "page container__with-bg-shapes">
        <Grid container
         direction="row"
         justify="center"
         alignItems="center"
       >
         
          <Grid xs={12} sm={12} md={12} lg={12}>
          <InputLabel className={classes["input__label_title"]}>
            Note it
          </InputLabel>
          </Grid>
          <Grid xs={12} sm={12} md={9} lg={9} >
          <InputLabel className={classes["input__label_subtitle"]}>
            Mis Notas
          </InputLabel>
          </Grid>
          <Grid md={12} sm={12} lg={12}>
            <Divider className={classes["divider__withmargin"]} />
          </Grid>  
          <Grid md={12} sm={12} lg={12}>
          <InputLabel className={classes["select"]} id="demo-simple-select-helper-label">Libreta</InputLabel>
        <Select
          className={classes["select"]}
          labelId="demo-simple-select-helper-label"
          id="select"
          //value={}
          onChange={handleSelectNotebook}
          defaultValue="Sin Libreta"
        >
          <MenuItem value="Sin Libreta">
          <em>Sin Libreta</em>
          </MenuItem>
          {myNotebooks.map((notebook, index) => (
          <MenuItem key={index} value={notebook._id}>
            {notebook.titulo}
          </MenuItem>
          ))}
        </Select>
        </Grid>
          {notes.length ? notes.map((item, key) => (
            <Card className={classes.card__notes} key={item} >
              <CardHeader className={classes.card__header}
              onClick={() => handleDeleteNote(item)}
               action={
                <IconButton aria-label="settings">
                  <ClearIcon />
                </IconButton>
              }
              />
              <CardContent onClick={() => handClickOnCard(item)}> 
                <Typography  component="h2" >
                  {item.titulo}
                </Typography>
                <Typography  color="textSecondary" >
                {item.descripcion?.length > maxString ? item.descripcion.substring(0, maxString): item.descripcion}
                </Typography>
              </CardContent>
            </Card>
            )):<h6 className={classes.withMargin}>Sin Notas para esta libreta</h6>}
            <Grid md={12}>
            </Grid>
        </Grid>
        </div>
    );
}

function refreshPage() {
  window.setTimeout(function(){
      window.location.reload(false);
  }, 1500)
  
}

export default  withRouter(MyNotes);