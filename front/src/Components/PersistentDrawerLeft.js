import React from "react";
import PropTypes from "prop-types";
import classNames from "classnames";
import { withStyles } from "@material-ui/core/styles";
import Drawer from "@material-ui/core/Drawer";
import CssBaseline from "@material-ui/core/CssBaseline";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Box from "@material-ui/core/Box"
import List from "@material-ui/core/List";
import Typography from "@material-ui/core/Typography";
import Divider from "@material-ui/core/Divider";
import IconButton from "@material-ui/core/IconButton";
import MenuIcon from "@material-ui/icons/Menu";
import ChevronLeftIcon from "@material-ui/icons/ChevronLeft";
import ChevronRightIcon from "@material-ui/icons/ChevronRight";
import AddIcon from '@material-ui/icons/Add';
import HomeIcon from '@material-ui/icons/Home';
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import Icon from '@material-ui/core/Icon';
import ListItemText from "@material-ui/core/ListItemText";
import { Button, InputLabel } from '@material-ui/core';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import Book from "@material-ui/icons/Book";
import Note from "@material-ui/icons/Note";
import Avatar from '@material-ui/core/Avatar';
import axios from 'axios';
import {Link} from 'react-router-dom'
import {urlBackend, findUser, findMyUser} from '../utils/constants';
import jwt_decode from 'jwt-decode';
import '../App.css'

const drawerWidth = 260;

const styles = (theme) => ({
  root: {
    display: "flex"
  },
  toolbar: {
    color: "black",
  },
  appBar: {
    boxShadow: "none",
    backgroundColor:"transparent",
    transition: theme.transitions.create(["margin", "width"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen
    })
  },
  appBarShift: {
    width: `calc(100% - ${drawerWidth}px)`,
    marginLeft: drawerWidth,
    transition: theme.transitions.create(["margin", "width"], {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen
    })
  },
  small: {
    width: theme.spacing(3),
    height: theme.spacing(3),
  },
  large: {
    width: theme.spacing(7),
    height: theme.spacing(7),
  },
  "input__label": {
    fontFamily: "Montserrat, sans-serif",
    fontWeight: 400,
    fontSize: 15,
    marginTop: 0,
    marginLeft: 10,
  },
  "input__label2": {
    fontFamily: "Montserrat, sans-serif",
    fontWeight: 400,
    fontSize: 11,
    marginTop: 0,
    marginLeft: 5,
  },
  menuButton: {
    marginLeft: 12,
    marginRight: 20
  },
  hide: {
    display: "none"
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0
  },
  drawerPaper: {
    width: drawerWidth,
    color: "black"
  },
  drawerHeader: {
    display: "flex",
    alignItems: "center",
    padding: "0 8px",
    justifyContent: "flex-end"
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing.unit * 3,
    transition: theme.transitions.create("margin", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen
    }),
    marginLeft: -drawerWidth
  },
  contentShift: {
    transition: theme.transitions.create("margin", {
      easing: theme.transitions.easing.easeOut,
      duration: theme.transitions.duration.enteringScreen
    }),
    marginLeft: 0
  },
  home: {
    position: 'absolute',
    bottom:0
  }
});

class PersistentDrawerLeft extends React.Component {
  constructor(props){
    super(props)
    this.state = {
      open: false,
      name: "Name"
    };
  }
  

  componentDidMount(){
    this.getName();
  }

  getName(){
    const email = jwt_decode(localStorage.getItem('jwt')).email;
    /*axios({
      method: 'get',
      url: urlBackend+findMyUser+encodeURIComponent(email),
      headers:{
        'Authorization': `Bearer ${localStorage.getItem("jwt")}`
      },
    })
    .then(res => {
      this.setState({name: res.data.name})
    })
    .catch(function (error) {
        console.log(error);
      });*/
      this.setState({name: email})
  }
  handleLogOut = () => {
    this.props.setJwt(null)
    localStorage.clear();
  }
  handleDrawerOpen = () => {
    this.props.setDrawerOpen(true);
    this.setState({ open: true });
  };

  handleDrawerClose = () => {
    this.props.setDrawerOpen(false);
    this.setState({ open: false });
  };

  render() {
    const { classes, theme } = this.props;
    const { open } = this.state;

    return (
      <div className={classes.root}>
        <AppBar
          position="fixed"
          className={classNames(classes.appBar, {
            [classes.appBarShift]: open
          })}
        >
          <Toolbar
            className={classNames(classes.toolbar, {})}
            disableGutters={!open}
          >
            <IconButton
              color="inherit"
              aria-label="Open drawer"
              onClick={this.handleDrawerOpen}
              className={classNames(classes.menuButton, open && classes.hide)}
            >
              <MenuIcon />
            </IconButton>
          </Toolbar>
        </AppBar>
        <Drawer
          className={classes.drawer}
          variant="persistent"
          anchor="left"
          open={open}
          classes={{
            paper: classes.drawerPaper
          }}
        >
          <div className={classes.drawerHeader}>
          <Box justifyContent="flex-start"><Avatar alt="Remy Sharp" src="/images/avatar.jpg" /></Box>
          <InputLabel className={classes["input__label2"]}> {this.state.name} </InputLabel>
            <IconButton onClick={this.handleDrawerClose}>
              {theme.direction === "ltr" ? (
                <ChevronLeftIcon />
              ) : (
                <ChevronRightIcon />
              )}
            </IconButton>
          </div>

          <Divider />
          {<List>
            {[
              "Nueva Nota",
              "Nueva Libreta",
            ].map((text, index) => (
              <Link className="menu__option" onClick={this.handleDrawerClose} to={index > 0 ? "newNotebook":"newNote"}>
                <ListItem button key={text}>
                  <AddIcon className="fa fa-plus-circle" color="primary">
                  </AddIcon>
                  <ListItemText primary={text} />
                </ListItem>
              </Link>
            ))}
            </List>}
          <Divider />
          <List>
            {[
              "Mis Notas",
              "Notas Compartidas",
              "Libretas",
              "Libretas Compartidas"
            ].map((text, index) => (
              <Link className="menu__option" onClick={this.handleDrawerClose} to={index === 0 ? "mynotes":"/"}>
              <ListItem button key={text}>
                <ListItemIcon color="black">
                  {index > 1 ? <Book /> : <Note />}
                </ListItemIcon>
                <ListItemText primary={text} />
              </ListItem>
              </Link>
            ))}
          </List>
          <Divider />
          <Link to="/" > <Button onClick={this.handleDrawerClose}><HomeIcon fontSize="large" color="primary"></HomeIcon></Button></Link>
        <Divider />
        <Link to="/" > <Button className={classNames(classes.home)} onClick={this.handleLogOut}><ExitToAppIcon fontSize="large" color="primary"></ExitToAppIcon> Logout</Button></Link>
        </Drawer>
      </div>
    );
  }
}

PersistentDrawerLeft.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired
};

export default withStyles(styles, { withTheme: true })(PersistentDrawerLeft);
