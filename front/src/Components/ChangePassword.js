import React,  { useState } from 'react';
import { withRouter } from "react-router-dom";

import { sha256 } from '../utils/crypto-utils';

import '../App.css';
import axios from 'axios';

import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import FormControl from '@material-ui/core/FormControl';
import FormHelperText from '@material-ui/core/FormHelperText';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';

import InputAdornment from '@material-ui/core/InputAdornment';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';

import { withStyles, makeStyles } from '@material-ui/core/styles';

function hashPassword(password) {
  return sha256(password);
}

const ColorButton = withStyles((theme) => ({
  root: {
    backgroundColor: "#2A4BC0",
    '&:hover': {
      backgroundColor: "#2A4BC0",
    },
    textTransform: 'capitalize',
    fontFamily: 'Montserrat, sans-serif',
    fontSize: "1.1rem",
    fontWeight: 300,
    width: '100%',
    padding: ".6rem 0",
    margin: '0 auto'
  },
}))(Button);

const useStyles = makeStyles((theme) => ({
  root: {
    '& > *': {
      margin: theme.spacing(1),
    },
  },
  "form__input-control": {
    width: "100%",
    marginBottom: ".6rem"
  },
  "form__input": {
    fontFamily: "Montserrat, sans-serif",
    fontWeight: 400
  },
}));

function ChangePassword(props){
  const classes = useStyles();
  const [newPassword, setNewPassword] = useState("");
  const [showNewPassword, setShowNewPassword] = useState(false);
  const [newPasswordErrorExists, setIfNewPasswordErrorExists] = useState(false);
  const [newPasswordErrorMessage, setNewPasswordErrorMessage] = useState("");
  const [confirmedNewPasswordValue, setConfirmedNewPasswordValue] = useState("");
  const [showConfirmedNewPasswordValue, setShowConfirmedNewPasswordValue] = useState(false);
  const [confirmedNewPasswordValueErrorExists, setIfConfirmedNewPasswordValueErrorExists] = useState(false);
  const [confirmedNewPasswordValueErrorMessage, setConfirmedNewPasswordValueErrorMessage] = useState("");

  const handleNewPasswordChange = (e) => {
    if (newPasswordErrorExists) {
      setIfNewPasswordErrorExists(false);
      setNewPasswordErrorMessage("");
    }
    if (confirmedNewPasswordValueErrorExists) {
      setIfConfirmedNewPasswordValueErrorExists(false);
      setConfirmedNewPasswordValueErrorMessage("");
    }
    setNewPassword(e.target.value);
  }

  const handleClickShowNewPassword = () => {
    setShowNewPassword(!showNewPassword);
  };

  const handleMouseDownNewPassword = (event) => {
    event.preventDefault();
  };

  const handleConfirmedNewPasswordValueChange = (e) => {
    if (confirmedNewPasswordValueErrorExists) {
      setIfConfirmedNewPasswordValueErrorExists(false);
      setConfirmedNewPasswordValueErrorMessage("");
    }
    setConfirmedNewPasswordValue(e.target.value);
  }

  const handleClickShowConfirmedNewPasswordValue = () => {
    setShowConfirmedNewPasswordValue(!showConfirmedNewPasswordValue);
  };

  const handleMouseDownConfirmedNewPasswordValue = (event) => {
    event.preventDefault();
  };

  const saveNewPassword = () => {
    if (newPassword.length < 10) {
      setIfNewPasswordErrorExists(true);
      setNewPasswordErrorMessage("La longitud de la nueva contraseña debe ser por lo menos de 10 caracteres.");
      return;
    }

    if (newPassword !== confirmedNewPasswordValue) {
      setIfConfirmedNewPasswordValueErrorExists(true);
      setConfirmedNewPasswordValueErrorMessage("Las contraseñas deben coincidir.");
      return;
    }

    alert(`new password: ${hashPassword(newPassword)}`);
    // TODO: guardar nueva contraseña
  };

  return(
    <div className="container__with-bg-shapes">
      <div className="card__container">
        <div className="card__header">
          <h1 className="card__title">Note It</h1>
          <div className="card__subtitle">
            <p className="card__subtitle-text">Cambiar contraseña</p>
          </div>
        </div>
        <div className="card__content">
          <form className={classes.root}>
            <FormControl
              className={classes["form__input-control"]}
              error={newPasswordErrorExists}
            >
              <InputLabel className={classes["form__input"]} htmlFor="new_password">Nueva contraseña</InputLabel>
              <Input
                id="new_password"
                className={classes["form__input"]}
                type={showNewPassword ? 'text' : 'password'}
                onChange={handleNewPasswordChange}
                endAdornment={
                  <InputAdornment position="end">
                    <IconButton
                      aria-label="toggle password visibility"
                      onClick={handleClickShowNewPassword}
                      onMouseDown={handleMouseDownNewPassword}
                    >
                      {showNewPassword ? <Visibility /> : <VisibilityOff />}
                    </IconButton>
                  </InputAdornment>
                }
              />
              {newPasswordErrorExists && <FormHelperText className={classes["form__input"]}>{newPasswordErrorMessage}</FormHelperText>}
            </FormControl>
            <FormControl
              className={classes["form__input-control"]}
              error={confirmedNewPasswordValueErrorExists}
            >
              <InputLabel className={classes["form__input"]} htmlFor="confirmed_new_password_value">Confirmar nueva contraseña</InputLabel>
              <Input
                id="confirmed_new_password_value"
                className={classes["form__input"]}
                type={showNewPassword ? 'text' : 'password'}
                onChange={handleConfirmedNewPasswordValueChange}
                endAdornment={
                  <InputAdornment position="end">
                    <IconButton
                      aria-label="toggle password visibility"
                      onClick={handleClickShowConfirmedNewPasswordValue}
                      onMouseDown={handleMouseDownConfirmedNewPasswordValue}
                    >
                      {showConfirmedNewPasswordValue ? <Visibility /> : <VisibilityOff />}
                    </IconButton>
                  </InputAdornment>
                }
              />
              {confirmedNewPasswordValueErrorExists && <FormHelperText className={classes["form__input"]}>{confirmedNewPasswordValueErrorMessage}</FormHelperText>}
            </FormControl>
            <ColorButton variant="contained" color="primary" className={classes.margin} onClick={() => saveNewPassword() }>
              Guardar
            </ColorButton>
          </form>
        </div>
      </div>
    </div>
  );
}

export default withRouter(ChangePassword);