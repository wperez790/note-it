import React,  { useState } from 'react';
import { withRouter, useHistory} from "react-router-dom";
import { MemoryRouter as Router } from 'react-router';

import { sha256 } from '../utils/crypto-utils';

import '../App.css';
import axios from 'axios';
// Material-ui
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import FormControl from '@material-ui/core/FormControl';
import FormHelperText from '@material-ui/core/FormHelperText';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import Grow from '@material-ui/core/Grow';
import Link from '@material-ui/core/Link'
import ArrowBackRounded from '@material-ui/icons/ArrowBackRounded';
import InputAdornment from '@material-ui/core/InputAdornment';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import { withStyles, makeStyles } from '@material-ui/core/styles';
// Constants
import {urlBackend, loginEndpoint} from '../utils/constants'


function emailIsValid(email) {
  return /^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(email)
}

const ColorButton = withStyles((theme) => ({
  root: {
    backgroundColor: "#2A4BC0",
    '&:hover': {
      backgroundColor: "#2A4BC0",
    },
    textTransform: 'capitalize',
    fontFamily: 'Montserrat, sans-serif',
    fontSize: "1.1rem",
    fontWeight: 300,
    width: '100%',
    padding: ".6rem 0",
    margin: '0 auto'
  },
}))(Button);

const useStyles = makeStyles((theme) => ({
  root: {
    '& > *': {
      margin: theme.spacing(1),
    },
  },
  "form__input-control": {
    width: "100%",
    marginBottom: ".6rem"
  },
  "form__input": {
    fontFamily: "Montserrat, sans-serif",
    fontWeight: 400
  },
  "link": {
    color: "#0288D1"
  },
  "label__register-question": {
    fontSize: ".8rem"
  }
}));

function Login(props){
  const classes = useStyles();
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [showPassword, setShowPassword] = useState(false);
  const [emailErrorExists, setIfEmailErrorExists] = useState(false);
  const [emailErrorMessage, setEmailErrorMessage] = useState("");
  const [passwordErrorExists, setIfPasswordErrorExists] = useState(false);
  const [verificationErrorExists, setVerificationErrorExists] = useState(false);
  const [passwordErrorMessage, setPasswordErrorMessage] = useState("");
  const [mayShowFirstStep, setMayShowFirstStep] = useState(true);
  const [showVerification, setShowVerification] = useState(false);
  const [verificationCode, setVerificationCode] = useState("");
  const history = useHistory();

  const handleEmailChange = (e) => {
    if (emailErrorExists) {
      setIfEmailErrorExists(false);
      setEmailErrorMessage("");
    }
    setEmail(e.target.value);
  }

  const handleCodeChange = (e) => {
    if(e.target.value.length === 10){
      setVerificationCode(e.target.value);
    }
  }
  const handlePasswordChange = (e) => {
    if (passwordErrorExists) {
      setIfPasswordErrorExists(false);
      setPasswordErrorMessage("");
    }
    setPassword(e.target.value);
  }

  const handleKeyPress = (e) => {
    if (e.key === 'Enter') {
      finishLogin()
    }
  }

  const handleClickShowPassword = () => {
    setShowPassword(!showPassword);
  };

  const handleMouseDownPassword = (event) => {
    event.preventDefault();
  };

  const handleEnter = (event) => {
    if (event.key === 'Enter') {
      goToLoginFinalStep()
    }
  }
  const goToLoginFinalStep = () => {
    
    if (email.length < 8) {
      setIfEmailErrorExists(true);
      setEmailErrorMessage("La longitud del email es muy corta. ¿Es una dirección valida?");
      return;
    }

    if (!emailIsValid(email)) {
      setIfEmailErrorExists(true);
      setEmailErrorMessage("El formato del email es incorrecto.");
      return;
    }

    setMayShowFirstStep(false);
  };

  const finishLogin = () => {
    if (password.length < 10) {
      setIfPasswordErrorExists(true);
      setPasswordErrorMessage("La longitud de la clave debe ser por lo menos de 10 caracteres.");
      return;
    }
    let data_ = {
      email: email, 
      password: password
    }
    if(Object.keys(verificationCode).length !== 0) {
      data_.verificationCode = verificationCode
    }
      axios({
        method: 'post',
        url: `${urlBackend}${loginEndpoint}`,
        data: data_
      })
      .then(res => {
        localStorage.setItem('jwt', res.data.token);
        props.setJwt(res.data.token);
        history.push("/")
      })
      .catch(function (error) {
        if(error.response && error.response.status === 400){
          setShowVerification(true); 
          return;
        }
        if(error.response && error.response.status === 401){
          setIfPasswordErrorExists(true)
          setPasswordErrorMessage("Incorrect User or Password");
          return;
        }
      });
    
    
  
    // TODO: loguearse al sistema
  };

  return(
    <div className="container__with-bg-shapes">
      <div className="card__container">
        <div className="card__header">
          <h1 className="card__title">Note It</h1>
          <div className="card__subtitle">
            {!mayShowFirstStep && 
              <div className="icon-back">
                <IconButton
                  onClick={() => { setMayShowFirstStep(true); setPassword("") }}
                  onMouseDown={handleMouseDownPassword}
                >
                  <ArrowBackRounded style={{color: "#2A4BC0"}} />
                </IconButton>
              </div>}
            <p className="card__subtitle-text">Acceder</p>
          </div>
        </div>
        <div className="card__content">
          <form className={classes.root}>
            <FormControl
              className={classes["form__input-control"]}
              error={emailErrorExists}
            >
              <InputLabel  className={classes["form__input"]} htmlFor="email">E-mail</InputLabel>
              <Input className={classes["form__input"]} id="email" onChange={handleEmailChange} disabled={!mayShowFirstStep} onKeyPress={handleEnter} />
              {emailErrorExists && <FormHelperText className={classes["form__input"]}>{emailErrorMessage}</FormHelperText>}
            </FormControl>
            {!mayShowFirstStep &&
              <Grow in={!mayShowFirstStep}>
                <FormControl
                  className={classes["form__input-control"]}
                  error={passwordErrorExists}
                >
                  <InputLabel className={classes["form__input"]} htmlFor="password">Clave</InputLabel>
                  <Input
                    id="standard-adornment-password"
                    className={classes["form__input"]}
                    type={showPassword ? 'text' : 'password'}
                    onChange={handlePasswordChange}
                    onKeyPress={handleKeyPress}
                    endAdornment={
                      <InputAdornment position="end">
                        <IconButton
                          aria-label="toggle password visibility"
                          onClick={handleClickShowPassword}
                          onMouseDown={handleMouseDownPassword}
                        >
                          {showPassword ? <Visibility /> : <VisibilityOff />}
                        </IconButton>
                      </InputAdornment>
                    }
                  />
                  {passwordErrorExists && <FormHelperText className={classes["form__input"]}>{passwordErrorMessage}</FormHelperText>}
                </FormControl>
              </Grow>
            }
            {mayShowFirstStep &&
              <ColorButton variant="contained" color="primary" className={classes.margin} onClick={() => goToLoginFinalStep() }>
                Continuar
              </ColorButton>
            }
            {showVerification && 
            <FormControl
            className={classes["form__input-control"]}
            error={verificationErrorExists}
            >
              <InputLabel className={classes["form__input"]} htmlFor="text">Codigo de Verificación</InputLabel>
              <Input
                id="standard-adornment-password"
                className={classes["form__input"]}
                onChange={handleCodeChange}
                />
            </FormControl>
            }
            {!mayShowFirstStep && 
              <ColorButton variant="contained" color="primary" className={classes.margin} onClick={() => finishLogin() }>
                Acceder
              </ColorButton>
            }
          </form>
        </div>
        <div className="card__footer">
          {!mayShowFirstStep &&
            <Grow
              in={!mayShowFirstStep}
            >
              <Router>
                <div style={{marginBottom: '2rem'}}>
                  <Link to="/password-recover" className={classes["link"]}>
                    ¿Olvidaste la contraseña?
                  </Link>
                </div>
                <p className={classes["label__register-question"]} style={{marginBottom: '.5rem'}}>¿No tienes una cuenta?</p>
                <div>
                  <Link href="/register" className={classes["link"]}>
                    Crear cuenta
                  </Link>
                </div>
              </Router>
            </Grow>
         }
        </div>
      </div>
    </div>
  );
}

export default withRouter(Login);