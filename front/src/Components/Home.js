import React from 'react';
import {useState, useEffect} from 'react';
import {Link, withRouter} from "react-router-dom";
import '../App.css';
import { useHistory } from "react-router-dom";
import { InputLabel, Divider, Card, Grid, Box, CardContent, Typography} from '@material-ui/core';
import axios from 'axios'
import moment from 'moment/min/moment-with-locales';
import {urlBackend, allNotes, maxString, noteEndpoint} from '../utils/constants'
import { withStyles, makeStyles } from '@material-ui/core/styles';
//const { ipcRenderer } = window.electron;

function parseDate(date){
  moment.locale('es')
  const date_moment = moment(date);
  return  date_moment.format('LLL');
}

const useStyles = makeStyles((theme) => ({
  "input__label_title": {
    fontFamily: "Montserrat, sans-serif",
    fontWeight: 400,
    fontSize:100,
    marginTop: 50
  },
  "input__label_subtitle": {
    fontFamily: "Montserrat, sans-serif",
    fontWeight: 400,
    fontSize:30,
    marginTop: 50
  },
  "divider__withmargin": {
    margin: 10
  },
  "card__notes": {
    cursor: "pointer",
    margin: 10,
    transition: "0.3s",
    boxShadow: "0 8px 40px rgba(0,0,0,0.3)",
    "&:hover": {
      boxShadow: "0 16px 70px rgba(0,0,0,0.3)"
    }
  },
}));


function Home (props){
  const classes = useStyles();
  const [notes, setNotes] = useState([]);
  const history = useHistory();

  useEffect(() => {
    getLastNotes();
  }, []);

  const handClickOnCard = async note => {
    await props.setNote(note)
    history.push("/notes")
  }
  const getLastNotes = async () => {
    let array = JSON.parse(localStorage.getItem("recentNotes")) || [];
    //await setNotes(array)
    let objArray = []
    array.forEach(async id => {
        axios({
          method: 'get',
          url: urlBackend + noteEndpoint + "/" + id ,
          headers:{
            'Authorization': `Bearer ${localStorage.getItem("jwt")}`
          }
        })
        .then(async res => {
        /* if(notes?.length === 0){
            objArray.push(res.data)
          }*/
          if(!objArray?.includes(res.data._id)){
            objArray.push(res.data)
            setNotes((prevArray) => ([...prevArray, ...[res.data]])) 
          }
        })
        .catch(function (error) {
          console.log(error);
        });
      
    }); 

  }
  /*const openNote = (id) => {
    history.push(`/note/${id}`)

    onClick={() => openNote(item.id)}
  }*/

  return (
    <div className= "page container__with-bg-shapes">
      <Grid container
       direction="row"
       justify="center"
       alignItems="center"
     >
       
        <Grid xs={12} sm={12} md={12} lg={12}>
        <InputLabel className={classes["input__label_title"]}>
          Note it
        </InputLabel>
        </Grid>
        <Grid xs={12} sm={12} md={9} lg={9} >
        <InputLabel className={classes["input__label_subtitle"]}>
          Notas recientes
        </InputLabel>
        </Grid>
        <Grid md={12} sm={12} lg={12}>
          <Divider className={classes["divider__withmargin"]} />
        </Grid>  
        {notes?.length? notes.map((item, key) => (
          <Card className={classes.card__notes} key={item._id} onClick={() => handClickOnCard(item)}>
            <CardContent>
              <Typography  component="h2" >
                {item.titulo}
              </Typography>
              <Typography  color="textSecondary" >
                {item.descripcion?.length > maxString ? item.descripcion.substring(0, maxString): item.descripcion}
              </Typography>
            </CardContent>
          </Card>
          )):null}
          <Grid md={12}>
          </Grid>
          <Grid xs={12} sm={12} md={9} lg={9} >
        <InputLabel className={classes["input__label_subtitle"]}>
          Comandos de acceso rápido
        </InputLabel>
        </Grid>
        <Grid md={12} sm={12} lg={12}>
          <Divider className={classes["divider__withmargin"]} />
        </Grid> 
        {props.drawerOpen?<Grid md={3} lg={3}></Grid>:null}
        <Grid md={9} lg={9}>
          <ul>
            <li>Alt+1: Sacar Note it de la bandeja</li>
            <li>Alt+2: Abrir Comandos rápidos</li>
            <li>Comando: note [nota]</li>
            <li>Comando: note -e</li>
          </ul>
        </Grid>   
      </Grid>

      

    </div>
  );
 
}

export default  withRouter(Home);