import React from 'react';
import {withRouter} from "react-router-dom";
import {Container, Row, Col} from 'react-bootstrap'
import '../App.css';
import axios from 'axios'
import moment from 'moment'
import { useEffect } from "react";
import swal from 'sweetalert';
import {urlBackend, noteEndpoint} from '../utils/constants'
const { ipcRenderer } = window.require('electron');



function parseDate(date) {
  moment.locale('es')
  const date_moment = moment(date);
  return  date_moment.format();
}

function Recorder(props) {

    
function handleKeyPress(target){
  const text = document.getElementById("code").value;
  if(target.charCode === 13){
    const textArray = text.split(" ")
    
    switch(textArray[0]){
      case 'note': 
          if(textArray[1] === "-e"){
            ipcRenderer.send('open-note-extended')
            ipcRenderer.send('hide-popup-window')
          }
          else{
            const cut = 5 + parseInt(textArray[1].length)
            axios({
              method: 'post',
              url: urlBackend+noteEndpoint,
              headers:{
                'Authorization': `Bearer ${localStorage.getItem("jwt")}`
              },
              data:{
                  'titulo': textArray[1],
                  'descripcion': text.substring(cut)
              }
          })
          .then(async res => {
            let nota = {}
            nota._id = res.data.id
            await props.setNote(nota)
            ipcRenderer.send('hide-popup-window')
          })
          .catch(function (error) {
            swal("Error", error.response.data, "error")
            });
          }
        break;
      default: swal("No existe el comando", "nota -e para abrir nota extendida\nnota [nota] para nota rápida", "info")
        break;
    }
  }
}

    useEffect(() => {
      //props.setHelp(false)
      props.setShowNav(false)
    })
    return(
      <div className= "page container__with-bg-shapes">
      <Container fluid>
        <br />
        <br />
        <h5>Command</h5>
        <Row>
          <Col sm = {{span: 8, offset:2}}>
          <input type= "text" id="code" autoFocus={true} onKeyPress={handleKeyPress}></input>
          </Col>
        </Row>
      </Container>
      </div>
    );
}

export default  withRouter (Recorder);