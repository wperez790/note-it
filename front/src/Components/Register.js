import React,  { useState } from 'react';
import { withRouter, Link as RouterLink  } from "react-router-dom";
import { MemoryRouter as Router } from 'react-router';

import { sha256 } from '../utils/crypto-utils';

import '../App.css';
import axios from 'axios';

import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import FormControl from '@material-ui/core/FormControl';
import FormHelperText from '@material-ui/core/FormHelperText';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import Avatar from '@material-ui/core/Avatar';
import Link from '@material-ui/core/Link';
import {urlBackend, registerEndpoint} from '../utils/constants'
import InputAdornment from '@material-ui/core/InputAdornment';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import ArrowBackRounded from '@material-ui/icons/ArrowBackRounded';
import { withStyles, makeStyles } from '@material-ui/core/styles';
import {useHistory} from 'react-router-dom'
import swal from 'sweetalert'
function emailIsValid(email) {
  return /^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(email)
}

const ColorButton = withStyles((theme) => ({
  root: {
    backgroundColor: "#2A4BC0",
    '&:hover': {
      backgroundColor: "#2A4BC0",
    },
    textTransform: 'capitalize',
    fontFamily: 'Montserrat, sans-serif',
    fontSize: "1.1rem",
    fontWeight: 300,
    width: '100%',
    padding: ".6rem 0",
    margin: '0 auto'
  },
}))(Button);

const useStyles = makeStyles((theme) => ({
  root: {
    '& > *': {
      margin: theme.spacing(1),
    },
  },
  "form__input-control": {
    width: "100%",
    marginBottom: ".6rem",
    "&:not(:last-child)": {
      marginRight: "1rem",
    }
  },
  "form__input": {
    fontFamily: "Montserrat, sans-serif",
    fontWeight: 400
  },
  "link": {
    color: "#0288D1"
  },
  "label__login-question": {
    fontSize: ".9rem"
  },
  "profile-image__input": {
    width: '5rem',
    height: '5rem',
    marginTop: '1rem',
    borderRadius: '50%',
    outline: 'none',
    cursor: 'pointer',
  },
}));

function Register(props){
  const classes = useStyles();
  const [email, setEmail] = useState("");
  const [name, setName] = useState("");
  const [lastName, setLastName] = useState("");
  const [password, setPassword] = useState("");
  const [profileImagePath, setProfileImagePath] = useState("");
  const [showPassword, setShowPassword] = useState(false);
  const [nameErrorExists, setIfNameErrorExists] = useState(false);
  const [nameErrorMessage, setNameErrorMessage] = useState("");
  const [lastNameErrorExists, setIfLastNameErrorExists] = useState(false);
  const [lastNameErrorMessage, setLastNameErrorMessage] = useState("");
  const [emailErrorExists, setIfEmailErrorExists] = useState(false);
  const [emailErrorMessage, setEmailErrorMessage] = useState("");
  const [passwordErrorExists, setIfPasswordErrorExists] = useState(false);
  const [passwordErrorMessage, setPasswordErrorMessage] = useState("");
  const [profileImagePathErrorExists, setIfProfileImagePathErrorExists] = useState(false);
  const [profileImagePathErrorMessage, setProfileImagePathErrorMessage] = useState("");
  const history = useHistory();
  const handleNameChange = (e) => {
    if (nameErrorExists) {
      setIfNameErrorExists(false);
      setNameErrorMessage("");
    }
    setName(e.target.value);
  }

  const handleLastNameChange = (e) => {
    if (lastNameErrorExists) {
      setIfLastNameErrorExists(false);
      setLastNameErrorMessage("");
    }
    setLastName(e.target.value);
  }

  const handleEmailChange = (e) => {
    if (emailErrorExists) {
      setIfEmailErrorExists(false);
      setEmailErrorMessage("");
    }
    setEmail(e.target.value);
  }

  const handlePasswordChange = (e) => {
    if (passwordErrorExists) {
      setIfPasswordErrorExists(false);
      setPasswordErrorMessage("");
    }
    setPassword(e.target.value);
  }

  const handleProfileImagePathChange = (e) => {
    if (profileImagePathErrorExists) {
      setIfProfileImagePathErrorExists(false);
      setProfileImagePathErrorMessage("");
    }
    setProfileImagePath(e.target.value);
  }

  const handleClickShowPassword = () => {
    setShowPassword(!showPassword);
  }

  const handleMouseDownPassword = (event) => {
    event.preventDefault();
  }

  const finishRegister = () => {
    let errorExists = false;
    if (name.length < 3) {
      setIfNameErrorExists(true);
      setNameErrorMessage("La longitud del nombre debe ser de más de 3 caracteres.");
      errorExists = true;
    }

    if (lastName.length < 3) {
      setIfLastNameErrorExists(true);
      setLastNameErrorMessage("La longitud del/los apellido/s debe ser de más de 3 caracteres.");
      errorExists = true;
    }

    if (email.length < 8) {
      setIfEmailErrorExists(true);
      setEmailErrorMessage("La longitud del email es muy corta. ¿Es una dirección valida?");
      errorExists = true;
    }

    if (!emailIsValid(email)) {
      setIfEmailErrorExists(true);
      setEmailErrorMessage("El formato del email es incorrecto.");
      errorExists = true;
    }

    if (password.length < 10) {
      setIfPasswordErrorExists(true);
      setPasswordErrorMessage("La longitud de la clave debe ser por lo menos de 10 caracteres.");
      errorExists = true;
    }

    if (errorExists) {
      return;
    }

    axios({
      method: 'post',
      url: `${urlBackend}${registerEndpoint}`,
      data: {
        name: name,
        surname: lastName,
        email: email, 
        password: password
      },
    })
    .then(res => {
      swal("Registration Successful", "Verification email sended", "success");
    })
    .catch(function (error) {
      if (error.response.status === 400){
        swal("User Exists", error.response.data , "error");
      }
      if (error.response.status === 500){
        swal("Error", error.response.data , "error");
      }
    });
  
    // TODO: registrar en el sistema
  }

  return(
    <div className="container__with-bg-shapes">
      <div className="card__container-big">
        <div className="card__header">
          <h1 className="card__title">Note It</h1>
          <div className="card__subtitle">
          <div className="icon-back">
                <IconButton
                  onClick={() => {history.push("/login")}}
                >
                  <ArrowBackRounded style={{color: "#2A4BC0"}} />
                </IconButton>
              </div>
            <p className="card__subtitle-text">Crear cuenta</p>
          </div>
        </div>
        <div className="card__content">
          <form className={classes.root}>
            <div className="card__content-row-flex-start">
              <div className="card__content-column main-column">
                <div className="card__content-row-flex-space-evenly">
                  <FormControl
                    className={classes["form__input-control"]}
                    error={nameErrorExists}
                  >
                    <InputLabel className={classes["form__input"]} htmlFor="name">Nombre *</InputLabel>
                    <Input className={classes["form__input"]} id="name" onChange={handleNameChange} />
                    {nameErrorExists && <FormHelperText className={classes["form__input"]}>{nameErrorMessage}</FormHelperText>}
                  </FormControl>
                </div>
                <div className="card__content-row-flex-space-evenly">
                  <FormControl
                    className={classes["form__input-control"]}
                    error={lastNameErrorExists}
                  >
                    <InputLabel className={classes["form__input"]} htmlFor="name">Apellidos *</InputLabel>
                    <Input className={classes["form__input"]} id="name" onChange={handleLastNameChange} />
                    {lastNameErrorExists && <FormHelperText className={classes["form__input"]}>{lastNameErrorMessage}</FormHelperText>}
                  </FormControl>
                </div>
                <div className="card__content-row-flex-space-evenly">
                  <FormControl
                    className={classes["form__input-control"]}
                    error={emailErrorExists}
                  >
                    <InputLabel className={classes["form__input"]} htmlFor="name">E-mail *</InputLabel>
                    <Input className={classes["form__input"]} id="email" onChange={handleEmailChange} />
                    {emailErrorExists && <FormHelperText className={classes["form__input"]}>{emailErrorMessage}</FormHelperText>}
                  </FormControl>
                </div>
                <div className="card__content-row-flex-space-evenly">
                  <FormControl
                    className={classes["form__input-control"]}
                    error={passwordErrorExists}
                  >
                    <InputLabel className={classes["form__input"]} htmlFor="password">Clave *</InputLabel>
                    <Input
                      id="password"
                      className={classes["form__input"]}
                      type={showPassword ? 'text' : 'password'}
                      onChange={handlePasswordChange}
                      endAdornment={
                        <InputAdornment position="end">
                          <IconButton
                            aria-label="toggle password visibility"
                            onClick={handleClickShowPassword}
                            onMouseDown={handleMouseDownPassword}
                          >
                            {showPassword ? <Visibility /> : <VisibilityOff />}
                          </IconButton>
                        </InputAdornment>
                      }
                    />
                    {passwordErrorExists && <FormHelperText className={classes["form__input"]}>{passwordErrorMessage}</FormHelperText>}
                  </FormControl> 
                </div>
              </div>
              <div className="card__content-column fixed-size-column">
                <InputLabel className={classes["form__input"]} htmlFor="profileImage">Imágen de perfil</InputLabel>
                <Avatar id="profileImage" src="../images/profile-user-avatar-placeholder.svg" className={classes["profile-image__input"]}>
                </Avatar>
              </div>
            </div>
            <div className="card__content-row-flex-start">
              <div className="card__content-column main-column">
                <div className="card__content-row-flex-start">
                  <ColorButton variant="contained" color="primary" style={{ marginBottom: "1rem" }} onClick={() => finishRegister() }>
                    Registrarse
                  </ColorButton>
                </div>
                <div className="card__content-row-flex-start">
                  <Router>
                    <p className={classes["label__login-question"]} style={{ marginRight: ".5rem" }}>¿Ya tienes una cuenta?</p>
                    <div>
                      <Link component={RouterLink} to="/login" className={classes["link"]}>
                        Ingresa
                      </Link>
                    </div>
                  </Router>
                </div>
                <div className="card__content-row-flex-start" style={{ marginTop: '2rem' }}>
                  <span style={{ fontSize: '.9rem', color: '#f33' }}>* Campo obligatorio</span>
                </div>
              </div>
              <div className="card__content-column fixed-size-column">
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  )
}

export default withRouter(Register);