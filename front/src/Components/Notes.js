  
import React from 'react';
import {useState, useEffect} from 'react';
import {Link, withRouter} from "react-router-dom";
import '../App.css';
import { useHistory } from "react-router-dom";
import axios from 'axios'
import AddIcon from '@material-ui/icons/AddCircleOutlineRounded';
import { withStyles, makeStyles } from '@material-ui/core/styles';
import {Input, InputLabel, Divider, Card, Grid, Box, CardContent, Typography, Button} from '@material-ui/core';
import IconButton from '@material-ui/core/IconButton';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import MenuItem from '@material-ui/core/MenuItem';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import {urlBackend, notebookEndpoint, noteEndpoint, findUser} from '../utils/constants';
import ArrowBackRounded from '@material-ui/icons/ArrowBackRounded';
import swal from 'sweetalert'
import { NoteAdd } from '@material-ui/icons';
import Autocomplete from '@material-ui/lab/Autocomplete';
const { ipcRenderer } = window.require('electron');

const useStyles = makeStyles((theme) => ({
  root: {
    '& > *': {
      margin: theme.spacing(1),
    },
  },
  "form__input-control": {
    width: "100%",
    marginBottom: ".6rem"
  },
  "form__input": {
    fontFamily: "Montserrat, sans-serif",
    fontWeight: 400
  },
  "link": {
    color: "#0288D1"
  },
  "label__register-question": {
    fontSize: ".8rem"
  },
  textField: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
    width: '25ch',
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
    justifyContent: 'flex-start'
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
  "card__notes": {
    margin: 10,
    backgroundColor: "lightgray",
  },
}));

 function Notes(props){
  console.log(props.note)
  const classes = useStyles();
  const history = useHistory();
  const [text, setText] = useState("text")
  const [startDate, setStartDate] = useState(new Date())
  const [endDate, setEndDate] = useState(new Date())
  const [checkArray, setCheckArray] = useState(props.note.checklist? props.note.checklist:[])
  const [myNotebooks, setMyNotebooks] = useState([])
  const [note, setNote] = useState(props.note);
  const [users, setUsers] = useState([])
  const [userSelected, setUserSelected] = useState({})
  const [arrayParticipantes, setArrayParticipantes] = useState([])
  const [participantesToShow, setParticipantesToShow] = useState(props.note.participantes? props.note.participantes:[])
  const [notebookSelected, setNotebookSelected] = useState("")

  function handleAddCheck(){
    let newcheck = [{"desc":"Nuevo Check", "value": false}]
    setCheckArray(prevArray => ([...prevArray, ...newcheck]))
  }
  function handleSelectNotebook(e){
    setNotebookSelected(e.target.value)
  }
  function handleSubmit(event){
    event.preventDefault()
    const form = event.target.elements
    let data_= 
    {
      '_id': note._id,
    }
    if(note.titulo !== form.titulo.value){
      data_.titulo = form.titulo.value
    }
    if(note.descripcion !== form.titulo.descripcion){
      data_.descripcion = form.descripcion.value
    }
    if(arrayParticipantes?.length > 0){
      data_.participantes = arrayParticipantes
    }
    if(notebookSelected){
      data_.id_libreta = notebookSelected
    }
    if(checkArray?.length > 0){
      data_.checklist = checkArray
    }
    axios({
        method: 'put',
        url: urlBackend+noteEndpoint,
        headers:{
          'Authorization': `Bearer ${localStorage.getItem("jwt")}`
        },
        data: data_,
    })
    .then(res => {
      swal("Nota Modificada con éxito", "Se modifico correctamente", "success")
        //ipcRenderer.send('hide-window')
    })
    .catch(function (error) {
        swal("Error al crear la nota", error.response.data, "error")
      });
    
}
function handleCheck(index){
  let newCheckArray = [...checkArray];
  let newCheck = checkArray[index];
  newCheck.value = !newCheck.value;
  newCheckArray[index] = newCheck
  setCheckArray(newCheckArray)
}
function handleCheckDesc(e, index){
  const desc = e.target.value
  let newCheckArray = [...checkArray];
  let newCheck = checkArray[index];
  newCheck.desc = desc;
  newCheckArray[index] = newCheck
  setCheckArray(newCheckArray)

}
async function handleChangeUser(e){
  let name = e.target.value
  if(name.length >= 3){
    await axios({
      method: 'get',
      url: urlBackend + findUser + name,
      headers:{
        'Authorization': `Bearer ${localStorage.getItem("jwt")}`
      }
    })
    .then(async res => {
        await setUsers(res.data)
    })
    .catch(function (error) {
        console.log(error);
      });
    }
}

async function handleSelectUser(id){
  await setUserSelected(id)
}

function getNotebooks(){
  axios({
    method: 'get',
    url: urlBackend + notebookEndpoint,
    headers:{
      'Authorization': `Bearer ${localStorage.getItem("jwt")}`
    }
})
.then(res => {
    setMyNotebooks(res.data)
})
.catch(function (error) {
    console.log(error);
  });
}
const updateParticipantes= async () => {
  const participante = [{id: userSelected._id, permisos: ["UPDATE", "DELETE"]}]
  const participanteToShow = [{id: userSelected._id, name:`${userSelected.name} ${userSelected.surname}`}] 
  setArrayParticipantes(prevArray => ([...prevArray, ...participante]))
  setParticipantesToShow(prevArray => ([...prevArray, ...participanteToShow]))
}
useEffect(() => {
  getNotebooks();
  if(Object.keys(userSelected).length !== 0 ){
    updateParticipantes();
  }
}, [userSelected]);
  return(
    <div className= "page container__with-bg-shapes">
      <div className="card__container">
        <div className="card__header">
          <h1 className="card__title">Note it</h1>
          <div className="card__subtitle">
          <div className="icon-back">
                <IconButton
                  onClick={() => {history.goBack()}}
                >
                  <ArrowBackRounded style={{color: "#2A4BC0"}} />
                </IconButton>
              </div>
            </div>
          <form onSubmit={handleSubmit}>
          <InputLabel className={classes["form__input"]} htmlFor="">Título</InputLabel>
                  <Input
                    id="titulo"
                    className={classes["form__input"]}
                    defaultValue={note.titulo}
                    type='text'
                    />
           <TextField
          id="descripcion"
          label="Descripción"
          multiline={true}
          rowsMax={4}
          style={{ margin: 8 }}
          defaultValue={note.descripcion}
          helperText=""
          fullWidth
          margin="normal"
          InputLabelProps={{
            shrink: true,
          }}
          variant="filled"
         /> 
        <FormControl className={classes.formControl}>
        <InputLabel id="demo-simple-select-helper-label">Libreta</InputLabel>
        <Select
          labelId="demo-simple-select-helper-label"
          id="demo-simple-select-helper"
          defaultValue={note.id_libreta}
          onChange={handleSelectNotebook}
        >
          <MenuItem value="Sin asignar">
          <em>Sin asignar</em>
          </MenuItem>
          {myNotebooks.map((notebook, index) => (
          <MenuItem value={notebook._id}>
            {notebook.titulo}
          </MenuItem>
          ))}
        </Select>
        <FormHelperText>Seleccione si quiere asignar la nota a una libreta</FormHelperText>
      </FormControl>
    <Grid md={12} lg={12} sm={12}>
        <Autocomplete
            id="combo-box-demo"
            options={users}
            getOptionLabel={(option) => `${option.name} ${option.surname}`}
            style={{ width: "auto" }}
            getOptionSelected={(option) => handleSelectUser(option)}
            renderInput={(params) => <TextField {...params} label="Participantes" variant="outlined" onChange={handleChangeUser} />}
          />
      </Grid>
      {participantesToShow.length? participantesToShow.map((participante, index) => (
        <div>
        <Card className={classes.card__notes} key={index}>
          <CardContent>
            <Typography  component="h2" >
            {participante.id}
            </Typography>
          </CardContent>
        </Card></div>
      )):null}

  <InputLabel className={classes["form__input"]} htmlFor="">CheckList</InputLabel>
  <Button onClick={() => handleAddCheck()}><AddIcon color="primary" fontSize="large"></AddIcon>Nuevo Check</Button>
  <br />
    {checkArray?.map((check, index) => (
      <Grid md={12} lg={12} sm={12}>
      <FormControlLabel
        control={
          <Checkbox
            checked={check.value}
            onChange={() => handleCheck(index)}
            name="checks"
            color="primary"
          />
        }
      />
       <Input
        id={index}
        className={classes["form__input"]}
        type='text'
        placeholder= {check.desc}
        defaultValue= {check.desc}
        onChange={(event) => handleCheckDesc(event, index)}
        />

      
        </Grid>
    ))}
    <Grid>
        <Button
          fullWidth
          variant="contained"
          color="primary"
          type="submit"
        >
          Editar
        </Button>
      </Grid>
      
    </form>           
    </div>
    </div>
    </div>
  );
 }

export default  withRouter (Notes);