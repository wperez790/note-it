import React, {useState, useEffect} from 'react';
import logo from './logo.svg';
import './App.css';
import {
  Switch,
  Route,
  Redirect,
} from "react-router-dom";
import {CSSTransition, TransitionGroup} from 'react-transition-group';
import Home from './Components/Home';
// import About from './Components/About'
import NewNote from './Components/NewNote'
import Recorder from './Components/Recorder'
import Login from './Components/Login';
import Register from './Components/Register';
import ChangePassword from './Components/ChangePassword';
// import PasswordRecover from './Components/PasswordRecover'
import Notes from './Components/Notes'
import MyNotes from './Components/MyNotes'

// import NewEvent from './Components/NewEvent'
import axios from 'axios'
import moment from 'moment'
import PersistentDrawerLeft from './Components/PersistentDrawerLeft';
import NewNotebook from './Components/NewNotebook';
import { noteEndpoint, urlBackend } from './utils/constants';
const { ipcRenderer } = window.require('electron');


function App() {
  const [help, setHelp] = useState(true)
  const [jwt, setJwt] = useState(localStorage.getItem('jwt'))
  const [drawerOpen, setDrawerOpen] = useState(false)
  const [showNav, setShowNav] = useState(true)
  const [note, setNote] = useState({})

  const updateRecentNotes = () =>{
    let array = JSON.parse(localStorage.getItem("recentNotes")) || []
      if(array?.length > 6){
        array= array.reverse().pop().reverse()
      }
      /* Si existe el objeto note && no existe el objecto en el array  OR array vacio*/
      if(Object.keys(note).length !== 0) {
        /*if(array?.length === 0 ){
          array.push(note._id)
        }
        else {*/
          if(!array?.includes(note._id)){
            array.push(note._id)
          }
        }
        localStorage.setItem("recentNotes", JSON.stringify(array));
      //}
  }
  useEffect(() => {
   updateRecentNotes()
  }, [note]);
  return (
    <div className="App">
      {(jwt && showNav)? <PersistentDrawerLeft token={jwt} setJwt={(jwt) => setJwt(jwt)} setDrawerOpen={(drawerOpen) => setDrawerOpen(drawerOpen)} ></PersistentDrawerLeft>:null}
      <Route render={({location}) => (
        <TransitionGroup>
          <CSSTransition
            key = {location.key}
            timeout = {450}
            classNames = "fade"
            >
            <Switch location= {location}>
              <Route path="/login" component={() => <Login token={jwt} setJwt={(jwt) => setJwt(jwt)}/> } />
              <Route path="/register" component={() => <Register /> } />
              <Route path="/mynotes" component={() => <MyNotes setNote={(note) => setNote(note)}/>} />
              <Route path="/newNote" component={() => <NewNote setNote={(note) => setNote(note)}/>} />
              <Route path="/newNotebook" component={() => <NewNotebook />} />
              {/* <Route path="/recover_password" component={() => <PasswordRecover /> } />*/}
              <Route path="/change-password" component={() => <ChangePassword /> } />
              <Route path="/notes" component={() => <Notes note={note}/>} />
              <Route path="/recorder"  token={jwt} component={() => <Recorder token={jwt} setNote={(note) => setNote(note)} setShowNav={(showNav) => setShowNav(showNav)}/> } />
              {jwt? <Route path="/" component={() => <Home token={jwt} drawerOpen={drawerOpen} setNote={(note) => setNote(note)}/> } />:<Redirect to={`/login`} />}
            </Switch>
          </CSSTransition>
        </TransitionGroup>
      )}>
      </Route>
        {/* {help ? 
        <div>
        <h5>Manejo de Recorder</h5>
          <h6>
            <ul>
            <li>Alt+1: Sacar a iuju! de la bandeja</li>
            <li>Alt+2: Abrir Recorder</li>
            <li>Escribir nota [nota a grabar] (nota rapida)</li>
            <li>Escribir nota -e (nota extendida)</li>
            <li>Escribir evento -d [fecha] -n [nombre] [descripcion] (evento rapido)</li>
            <li>Escribir evento -e (evento extendido)</li>
            <li>Escribir tarea -t [hora_recordatorio] [descripcion] (tarea rapida)</li>
            <li>Escribir tarea -e (tarea extendida)</li>
            </ul>
          </h6>
          </div>: null } */}
    </div>
  );
}

export default App;
